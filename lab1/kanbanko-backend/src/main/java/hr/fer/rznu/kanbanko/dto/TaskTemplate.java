package hr.fer.rznu.kanbanko.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class TaskTemplate {

    private String title;

    private String stage;

    private Long parentProjectId;
}
