package hr.fer.rznu.kanbanko.service.impl;

import hr.fer.rznu.kanbanko.model.Project;
import hr.fer.rznu.kanbanko.repository.ProjectRepository;
import hr.fer.rznu.kanbanko.service.ProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;

@Service
public class ProjectServiceJpa implements ProjectService {

    @Autowired
    private ProjectRepository projectRepository;

    @Override
    public Project add(Project project) {
        return projectRepository.save(project);
    }

    @Override
    public Project fetch(Long id) {
        return projectRepository.findById(id).orElse(null);
    }

    @Override
    public List<Project> fetchAll() {
        return projectRepository.findAll();
    }

    @Override
    public List<Project> fetchAllByOwnerId(Long ownerId) {
        return projectRepository.findAllByOwnerId(ownerId);
    }

    @Override
    public Project modify(Project project) {
        return projectRepository.save(project);
    }

    @Override
    public void delete(Long id) {
        projectRepository.deleteById(id);
    }
}
