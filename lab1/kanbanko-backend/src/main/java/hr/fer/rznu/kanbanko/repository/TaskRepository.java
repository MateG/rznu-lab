package hr.fer.rznu.kanbanko.repository;

import hr.fer.rznu.kanbanko.model.Task;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TaskRepository extends JpaRepository<Task, Long> {

    List<Task> findAllByParentProjectId(Long parentProjectId);
}
