package hr.fer.rznu.kanbanko.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ProjectTemplate {

    private String name;

    private String description;

    private String visibility;

    private Long ownerId;
}
