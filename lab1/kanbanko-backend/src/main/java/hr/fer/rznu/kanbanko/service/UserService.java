package hr.fer.rznu.kanbanko.service;

import hr.fer.rznu.kanbanko.model.User;

import java.util.List;

public interface UserService {

    User add(User user);

    User fetch(Long id);

    User fetch(String username);

    List<User> fetchAll();

    User modify(User user);

    void delete(Long id);
}
