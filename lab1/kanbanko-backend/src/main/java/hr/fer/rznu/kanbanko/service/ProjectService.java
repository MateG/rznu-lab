package hr.fer.rznu.kanbanko.service;

import hr.fer.rznu.kanbanko.model.Project;
import hr.fer.rznu.kanbanko.model.User;

import java.util.List;

public interface ProjectService {

    Project add(Project project);

    Project fetch(Long id);

    List<Project> fetchAll();

    List<Project> fetchAllByOwnerId(Long ownerId);

    Project modify(Project project);

    void delete(Long id);
}
