package hr.fer.rznu.kanbanko.service;

import hr.fer.rznu.kanbanko.model.Session;

import java.time.Instant;

public interface SessionService {

    Session fetch(String id);

    Session add(Session session);

    void delete(String id);

    void deleteAllOlderThan(Instant oldestRemaining);
}
