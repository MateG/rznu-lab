package hr.fer.rznu.kanbanko.controller.nested;

import hr.fer.rznu.kanbanko.auth.UserDetail;
import hr.fer.rznu.kanbanko.dto.TaskSummary;
import hr.fer.rznu.kanbanko.model.Project;
import hr.fer.rznu.kanbanko.service.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/projects")
public class ProjectTaskController {

    @Autowired
    private TaskService taskService;

    @GetMapping("/{id}/tasks")
    public List<TaskSummary> getAllTasksByProject(@PathVariable Long id, Authentication auth) {
        Long authUserId = ((UserDetail) auth.getPrincipal()).getId();
        return taskService.fetchAllByParentProjectId(id)
                .stream()
                .filter(task -> task.getParentProject().getVisibility() == Project.Visibility.PUBLIC
                        || task.getParentProject().getOwner().getId().equals(authUserId))
                .map(TaskSummary::fromTask)
                .collect(Collectors.toList());
    }
}
