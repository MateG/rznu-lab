package hr.fer.rznu.kanbanko.service;

import hr.fer.rznu.kanbanko.model.Task;

import java.util.List;

public interface TaskService {

    Task add(Task task);

    Task fetch(Long id);

    List<Task> fetchAll();

    List<Task> fetchAllByParentProjectId(Long parentProjectId);

    Task modify(Task project);

    void delete(Long id);
}
