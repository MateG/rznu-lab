package hr.fer.rznu.kanbanko.controller;

import hr.fer.rznu.kanbanko.auth.UserDetail;
import hr.fer.rznu.kanbanko.model.User;
import hr.fer.rznu.kanbanko.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.List;

@RestController
@RequestMapping("/users")
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @PostMapping("")
    public ResponseEntity<String> createUser(@RequestBody User user) {
        user.setId(null);
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        User created = userService.add(user);
        return ResponseEntity.created((URI.create("/" + created.getId()))).body("Created at given location");
    }

    @GetMapping("")
    public List<User> getAllUsers() {
        return userService.fetchAll();
    }

    @GetMapping("/{id}")
    public ResponseEntity<User> getUser(@PathVariable Long id) {
        User user = userService.fetch(id);
        return user == null ? ResponseEntity.notFound().build() : ResponseEntity.ok(user);
    }

    @PutMapping("/{id}")
    public ResponseEntity<String> putUser(@PathVariable Long id, @RequestBody User user, Authentication auth) {
        user.setId(id);
        user.setPassword(passwordEncoder.encode(user.getPassword()));

        // Check if this is PUT for resource creation
        User existingUser = userService.fetch(id);
        if (existingUser == null) {
            User created = userService.add(user);
            return ResponseEntity.created((URI.create("/" + created.getId()))).body("Created at given location");
        }

        // Check if existing user matches the principal
        Long authUserId = ((UserDetail) auth.getPrincipal()).getId();
        if (!existingUser.getId().equals(authUserId)) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
        }

        userService.modify(user);
        return ResponseEntity.noContent().build();
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteUser(@PathVariable Long id, Authentication auth) {
        Long authUserId = ((UserDetail) auth.getPrincipal()).getId();
        if (!authUserId.equals(id)) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
        }
        userService.delete(id);
        return ResponseEntity.noContent().build();
    }
}
