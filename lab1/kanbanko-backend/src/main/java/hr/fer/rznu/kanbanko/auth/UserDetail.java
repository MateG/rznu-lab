package hr.fer.rznu.kanbanko.auth;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class UserDetail {

    private Long id;

    private String username;

    private String firstName;

    private String lastName;
}
