package hr.fer.rznu.kanbanko.controller;

import hr.fer.rznu.kanbanko.auth.UserDetail;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/auth")
public class AuthController {

    @GetMapping("")
    @PreAuthorize("isFullyAuthenticated()")
    public UserDetail authenticate(@AuthenticationPrincipal UserDetail userDetail) {
        return userDetail;
    }
}
