package hr.fer.rznu.kanbanko.dto;

import hr.fer.rznu.kanbanko.model.Project;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ProjectSummary {

    private Long id;

    private String name;

    private String description;

    private Project.Visibility visibility;

    private Long ownerId;

    public static ProjectSummary fromProject(Project project) {
        return builder()
                .id(project.getId())
                .name(project.getName())
                .description(project.getDescription())
                .visibility(project.getVisibility())
                .ownerId(project.getOwner().getId())
                .build();
    }
}
