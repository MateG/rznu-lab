package hr.fer.rznu.kanbanko.service.impl;

import hr.fer.rznu.kanbanko.model.Session;
import hr.fer.rznu.kanbanko.repository.SessionRepository;
import hr.fer.rznu.kanbanko.service.SessionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.time.Instant;

@Service
public class SessionServiceJpa implements SessionService {

    @Autowired
    private SessionRepository sessionRepository;

    @Override
    public Session fetch(String id) {
        return sessionRepository.findById(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }

    @Override
    public Session add(Session session) {
        return sessionRepository.save(session);
    }

    @Override
    public void delete(String id) {
        sessionRepository.deleteById(id);
    }

    @Override
    public void deleteAllOlderThan(Instant oldestRemaining) {
        sessionRepository.deleteAllByValidUntilBefore(oldestRemaining);
    }
}
