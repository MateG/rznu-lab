package hr.fer.rznu.kanbanko.controller;

import hr.fer.rznu.kanbanko.auth.AuthCookieFilter;
import hr.fer.rznu.kanbanko.auth.UserDetail;
import hr.fer.rznu.kanbanko.dto.LoginInfo;
import hr.fer.rznu.kanbanko.model.Session;
import hr.fer.rznu.kanbanko.model.User;
import hr.fer.rznu.kanbanko.service.SessionService;
import hr.fer.rznu.kanbanko.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseCookie;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.Duration;
import java.time.Instant;
import java.time.temporal.ChronoUnit;

@RestController
@RequestMapping("/login")
public class LoginController {

    @Autowired
    private UserService userService;

    @Autowired
    private SessionService sessionService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @PostMapping("")
    public ResponseEntity<UserDetail> login(@RequestBody LoginInfo loginInfo) {
        User user = userService.fetch(loginInfo.getUsername());
        if (passwordEncoder.matches(loginInfo.getPassword(), user.getPassword())) {
            Instant validUntil = Instant.now().plus(1, ChronoUnit.HOURS);
            Session session = Session.builder().user(user).validUntil(validUntil).build();
            sessionService.add(session);

            ResponseCookie cookie = ResponseCookie.from(AuthCookieFilter.COOKIE_NAME, session.getId())
                    .maxAge(Duration.ofHours(1))
                    .path("/")
                    .httpOnly(true)
                    .sameSite("Strict")
                    .build();
            UserDetail userDetail = new UserDetail(
                    user.getId(),
                    user.getUsername(),
                    user.getFirstName(),
                    user.getLastName()
            );
            return ResponseEntity.ok().header(HttpHeaders.SET_COOKIE, cookie.toString()).body(userDetail);
        }

        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
    }
}
