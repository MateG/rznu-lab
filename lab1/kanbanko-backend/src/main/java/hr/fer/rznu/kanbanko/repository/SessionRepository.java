package hr.fer.rznu.kanbanko.repository;

import hr.fer.rznu.kanbanko.model.Session;
import org.springframework.data.repository.CrudRepository;

import java.time.Instant;

public interface SessionRepository extends CrudRepository<Session, String> {

    void deleteAllByValidUntilBefore(Instant oldestRemaining);
}
