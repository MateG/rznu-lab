package hr.fer.rznu.kanbanko.auth;

import hr.fer.rznu.kanbanko.repository.SessionRepository;
import hr.fer.rznu.kanbanko.service.SessionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class SessionCleanupLogoutSuccessHandler implements LogoutSuccessHandler {

    @Autowired
    private SessionService sessionService;

    @Override
    public void onLogoutSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) {
        String sessionId = AuthCookieFilter.extractAuthenticationCookie(request);
        if (sessionId != null) {
            sessionService.delete(sessionId);
        }
    }
}
