package hr.fer.rznu.kanbanko.auth;

import hr.fer.rznu.kanbanko.model.Session;
import hr.fer.rznu.kanbanko.model.User;
import hr.fer.rznu.kanbanko.service.SessionService;
import hr.fer.rznu.kanbanko.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Arrays;

@Component
public class AuthCookieFilter extends GenericFilterBean {

    public static final String COOKIE_NAME = "authentication";

    @Autowired
    private SessionService sessionService;

    @Autowired
    private UserService userService;

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        String sessionId = extractAuthenticationCookie((HttpServletRequest) request);

        if (sessionId != null) {
            try {
                Session session = sessionService.fetch(sessionId);
                User user = session.getUser();
                UserDetail userDetail = new UserDetail(
                        user.getId(),
                        user.getUsername(),
                        user.getFirstName(),
                        user.getLastName()
                );
                SecurityContextHolder.getContext().setAuthentication(new UserAuthentication(userDetail));
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }

        chain.doFilter(request, response);
    }

    public static String extractAuthenticationCookie(HttpServletRequest request) {
        Cookie[] cookies = request.getCookies();
        if (cookies == null) return null;
        return Arrays.stream(cookies)
                .filter(c -> c.getName().equals(COOKIE_NAME))
                .findFirst()
                .map(Cookie::getValue)
                .orElse(null);
    }
}
