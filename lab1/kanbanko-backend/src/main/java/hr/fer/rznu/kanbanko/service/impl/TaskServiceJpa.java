package hr.fer.rznu.kanbanko.service.impl;

import hr.fer.rznu.kanbanko.model.Task;
import hr.fer.rznu.kanbanko.repository.TaskRepository;
import hr.fer.rznu.kanbanko.service.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@Service
public class TaskServiceJpa implements TaskService {

    @Autowired
    private TaskRepository taskRepository;

    @Override
    public Task add(Task task) {
        return taskRepository.save(task);
    }

    @Override
    public Task fetch(Long id) {
        return taskRepository.findById(id).orElse(null);
    }

    @Override
    public List<Task> fetchAll() {
        return taskRepository.findAll();
    }

    @Override
    public List<Task> fetchAllByParentProjectId(Long parentProjectId) {
        return taskRepository.findAllByParentProjectId(parentProjectId);
    }

    @Override
    public Task modify(Task project) {
        return taskRepository.save(project);
    }

    @Override
    public void delete(Long id) {
        taskRepository.deleteById(id);
    }
}
