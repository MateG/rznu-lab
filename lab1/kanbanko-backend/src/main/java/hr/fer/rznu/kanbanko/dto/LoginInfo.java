package hr.fer.rznu.kanbanko.dto;

import lombok.Data;

@Data
public class LoginInfo {

    private String username;

    private String password;
}
