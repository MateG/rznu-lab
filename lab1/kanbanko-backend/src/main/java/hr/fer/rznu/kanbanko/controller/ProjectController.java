package hr.fer.rznu.kanbanko.controller;

import hr.fer.rznu.kanbanko.auth.UserDetail;
import hr.fer.rznu.kanbanko.dto.ProjectTemplate;
import hr.fer.rznu.kanbanko.model.Project;
import hr.fer.rznu.kanbanko.model.User;
import hr.fer.rznu.kanbanko.service.ProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/projects")
public class ProjectController {

    @Autowired
    private ProjectService projectService;

    @PostMapping("")
    public ResponseEntity<String> createProject(@RequestBody ProjectTemplate projectTemplate, Authentication auth) {
        // Check if owner ID from the request body matches the principal's ID
        Long authUserId = ((UserDetail) auth.getPrincipal()).getId();
        if (projectTemplate.getOwnerId() != null && !projectTemplate.getOwnerId().equals(authUserId)) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
        }

        Project created = projectService.add(buildProject(null, projectTemplate, authUserId));
        return ResponseEntity.created(URI.create("/" + created.getId())).body("Created at given location");
    }

    @GetMapping("")
    public List<Project> getAllProjects(Authentication auth) {
        Long authUserId = ((UserDetail) auth.getPrincipal()).getId();
        return projectService.fetchAll()
                .stream()
                .filter(project -> project.getVisibility() == Project.Visibility.PUBLIC
                        || project.getOwner().getId().equals(authUserId))
                .collect(Collectors.toList());
    }

    @GetMapping("/{id}")
    public ResponseEntity<Project> getProject(@PathVariable Long id, Authentication auth) {
        Project project = projectService.fetch(id);
        if (project == null) {
            return ResponseEntity.notFound().build();
        }

        Long authUserId = ((UserDetail) auth.getPrincipal()).getId();
        if (project.getVisibility() == Project.Visibility.PRIVATE
                && !(authUserId.equals(project.getOwner().getId()))) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
        }

        return ResponseEntity.ok(project);
    }

    @PutMapping("/{id}")
    public ResponseEntity<String> putProject(@PathVariable Long id, @RequestBody ProjectTemplate projectTemplate, Authentication auth) {
        // Check if owner ID from the request body matches the principal's ID
        Long authUserId = ((UserDetail) auth.getPrincipal()).getId();
        if (projectTemplate.getOwnerId() != null && !projectTemplate.getOwnerId().equals(authUserId)) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
        }

        // Check if this is PUT for resource creation
        Project existingProject = projectService.fetch(id);
        if (existingProject == null) {
            Project created = projectService.add(buildProject(id, projectTemplate, authUserId));
            return ResponseEntity.created(URI.create("/" + created.getId())).body("Created at given location");
        }

        // Check if existing project is owned by the principal
        if (!existingProject.getOwner().getId().equals(authUserId)) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
        }

        projectService.modify(buildProject(id, projectTemplate, authUserId));
        return ResponseEntity.noContent().build();
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteProject(@PathVariable Long id, Authentication auth) {
        Long authUserId = ((UserDetail) auth.getPrincipal()).getId();
        Project project = projectService.fetch(id);
        if (!authUserId.equals(project.getOwner().getId())) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
        }
        projectService.delete(id);
        return ResponseEntity.noContent().build();
    }

    private Project buildProject(Long id, ProjectTemplate projectTemplate, Long ownerId) {
        return Project.builder()
                .id(id)
                .name(projectTemplate.getName())
                .description(projectTemplate.getDescription())
                .visibility(Project.Visibility.valueOf(projectTemplate.getVisibility()))
                .owner(User.builder().id(ownerId).build())
                .build();
    }
}
