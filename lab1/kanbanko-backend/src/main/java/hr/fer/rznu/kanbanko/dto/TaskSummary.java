package hr.fer.rznu.kanbanko.dto;

import hr.fer.rznu.kanbanko.model.Task;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class TaskSummary {

    private Long id;

    private String title;

    private Task.Stage stage;

    private Long parentProjectId;

    public static TaskSummary fromTask(Task task) {
        return builder()
                .id(task.getId())
                .title(task.getTitle())
                .stage(task.getStage())
                .parentProjectId(task.getParentProject().getId())
                .build();
    }
}
