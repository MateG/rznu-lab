package hr.fer.rznu.kanbanko.repository;

import hr.fer.rznu.kanbanko.model.Project;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ProjectRepository extends JpaRepository<Project, Long> {

    List<Project> findAllByOwnerId(Long ownerId);
}
