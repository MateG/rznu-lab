package hr.fer.rznu.kanbanko.controller;

import hr.fer.rznu.kanbanko.auth.UserDetail;
import hr.fer.rznu.kanbanko.dto.TaskSummary;
import hr.fer.rznu.kanbanko.dto.TaskTemplate;
import hr.fer.rznu.kanbanko.model.Project;
import hr.fer.rznu.kanbanko.model.Task;
import hr.fer.rznu.kanbanko.service.ProjectService;
import hr.fer.rznu.kanbanko.service.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/tasks")
public class TaskController {

    @Autowired
    private TaskService taskService;

    @Autowired
    private ProjectService projectService;

    @PostMapping("")
    public ResponseEntity<String> createTask(@RequestBody TaskTemplate taskTemplate, Authentication auth) {
        Project parentProject = projectService.fetch(taskTemplate.getParentProjectId());
        if (parentProject == null) {
            return ResponseEntity.badRequest().body("Invalid parent project ID");
        }

        Long authUserId = ((UserDetail) auth.getPrincipal()).getId();
        if (!parentProject.getOwner().getId().equals(authUserId)) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
        }

        Task task = Task.builder()
                .title(taskTemplate.getTitle())
                .stage(Task.Stage.valueOf(taskTemplate.getStage()))
                .parentProject(Project.builder().id(taskTemplate.getParentProjectId()).build())
                .build();
        Task created = taskService.add(task);
        return ResponseEntity.created(URI.create("/" + created.getId())).body("Created at given location");
    }

    @GetMapping("")
    public List<TaskSummary> getAllTasks(Authentication auth) {
        Long authUserId = ((UserDetail) auth.getPrincipal()).getId();
        return taskService.fetchAll()
                .stream()
                .filter(task -> task.getParentProject().getVisibility() == Project.Visibility.PUBLIC
                        || task.getParentProject().getOwner().getId().equals(authUserId))
                .map(TaskSummary::fromTask)
                .collect(Collectors.toList());
    }

    @GetMapping("/{id}")
    public ResponseEntity<TaskSummary> getTask(@PathVariable Long id, Authentication auth) {
        Task task = taskService.fetch(id);
        if (task == null) {
            return ResponseEntity.notFound().build();
        }

        Long authUserId = ((UserDetail) auth.getPrincipal()).getId();
        if (task.getParentProject().getVisibility() == Project.Visibility.PRIVATE
                && !(authUserId.equals(task.getParentProject().getOwner().getId()))) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
        }

        return ResponseEntity.ok(TaskSummary.fromTask(task));
    }

    @PutMapping("/{id}")
    public ResponseEntity<String> putTask(@PathVariable Long id, @RequestBody TaskTemplate taskTemplate, Authentication auth) {
        // Check if this is PUT for resource creation
        Task existingTask = taskService.fetch(id);
        if (existingTask == null) {
            Task created = taskService.add(buildTask(id, taskTemplate));
            return ResponseEntity.created(URI.create("/" + created.getId())).body("Created at given location");
        }

        // Check if parent project owner is owned by the principal
        Long authUserId = ((UserDetail) auth.getPrincipal()).getId();
        if (!existingTask.getParentProject().getOwner().getId().equals(authUserId)) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
        }

        taskService.modify(buildTask(id, taskTemplate));
        return ResponseEntity.noContent().build();
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteTask(@PathVariable Long id, Authentication auth) {
        Task task = taskService.fetch(id);
        if (task == null) {
            return ResponseEntity.notFound().build();
        }

        Long authUserId = ((UserDetail) auth.getPrincipal()).getId();
        if (!authUserId.equals(task.getParentProject().getOwner().getId())) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
        }

        projectService.delete(id);
        return ResponseEntity.noContent().build();
    }

    private Task buildTask(Long id, TaskTemplate taskTemplate) {
        return Task.builder()
                .id(id)
                .title(taskTemplate.getTitle())
                .stage(Task.Stage.valueOf(taskTemplate.getStage()))
                .parentProject(Project.builder().id(taskTemplate.getParentProjectId()).build())
                .build();
    }
}
