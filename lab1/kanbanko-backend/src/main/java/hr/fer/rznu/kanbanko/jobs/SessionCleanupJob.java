package hr.fer.rznu.kanbanko.jobs;

import hr.fer.rznu.kanbanko.service.SessionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;

@Component
public class SessionCleanupJob {

    @Autowired
    private SessionService sessionService;

    @Transactional
    @Scheduled(cron = "${kanbanko.jobs.session-cleanup.cron}")
    public void cleanUpExpiredSessions() {
        sessionService.deleteAllOlderThan(Instant.now());
    }
}
