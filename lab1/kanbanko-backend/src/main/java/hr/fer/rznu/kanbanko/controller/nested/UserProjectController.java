package hr.fer.rznu.kanbanko.controller.nested;

import hr.fer.rznu.kanbanko.auth.UserDetail;
import hr.fer.rznu.kanbanko.dto.ProjectSummary;
import hr.fer.rznu.kanbanko.model.Project;
import hr.fer.rznu.kanbanko.service.ProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/users")
public class UserProjectController {

    @Autowired
    private ProjectService projectService;

    @GetMapping("/{id}/projects")
    public List<ProjectSummary> getAllProjectsByUser(@PathVariable Long id, Authentication auth) {
        Long authUserId = ((UserDetail) auth.getPrincipal()).getId();
        return projectService.fetchAllByOwnerId(id)
                .stream()
                .filter(project -> project.getVisibility() == Project.Visibility.PUBLIC
                        || project.getOwner().getId().equals(authUserId))
                .map(ProjectSummary::fromProject)
                .collect(Collectors.toList());
    }
}
