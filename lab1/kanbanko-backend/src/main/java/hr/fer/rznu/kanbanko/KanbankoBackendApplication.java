package hr.fer.rznu.kanbanko;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
public class KanbankoBackendApplication {

    public static void main(String[] args) {
        SpringApplication.run(KanbankoBackendApplication.class, args);
    }
}
