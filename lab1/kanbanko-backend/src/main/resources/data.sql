INSERT INTO USER (id, username, password, first_name, last_name)
VALUES (0, 'MateG', '$2y$12$FHnW9q1gn.usg4f0m4YML.fYDG5LVKpsODWE./UaRgtgKCelH42G6', 'Mate', 'Gasparini');
INSERT INTO USER (id, username, password, first_name, last_name)
VALUES (1, 'Pero', '$2y$12$UST8ndZpEP4iOuyHh8l26uMs170hONromXvYnGne8wCSp3NWuL0uu', 'Pero', 'Peric');

INSERT INTO PROJECT (id, name, description, visibility, owner_id)
VALUES (0, 'RZNU Lab1', 'First lab assignment for the ''Service-Oriented Computing'' course', 'PRIVATE', 0);
INSERT INTO PROJECT (id, name, description, visibility, owner_id)
VALUES (1, 'Master''s Project', 'Project which precedes the Master''s Thesis', 'PUBLIC', 0);
INSERT INTO PROJECT (id, name, description, visibility, owner_id)
VALUES (2, 'Pero''s Private Project', 'Project that only Pero has access to', 'PRIVATE', 1);
INSERT INTO PROJECT (id, name, description, visibility, owner_id)
VALUES (3, 'Pero''s Public Project', 'Project that everybody can see', 'PUBLIC', 1);

INSERT INTO TASK (id, title, stage, parent_project_id)
VALUES (0, 'Design database', 'DONE', 0);
INSERT INTO TASK (id, title, stage, parent_project_id)
VALUES (1, 'Design frontend', 'DONE', 0);
INSERT INTO TASK (id, title, stage, parent_project_id)
VALUES (2, 'Implement authentication', 'TEST', 0);
INSERT INTO TASK (id, title, stage, parent_project_id)
VALUES (3, 'Present the assignment', 'TO_DO', 0);