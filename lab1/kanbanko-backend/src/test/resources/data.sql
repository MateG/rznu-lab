DELETE FROM USER;

INSERT INTO USER (id, username, password, first_name, last_name)
VALUES(0, 'MateG', '$2y$12$FHnW9q1gn.usg4f0m4YML.fYDG5LVKpsODWE./UaRgtgKCelH42G6', 'Mate', 'Gasparini');

INSERT INTO SESSION (id, user_id, valid_until)
VALUES('AWaKQ1YDXIJnlGw2PvqyAAiARSkoFe+cd2QdAjU8e9I', 0, '2222-02-02 22:22:22.222222');

INSERT INTO PROJECT (id, name, description, visibility, owner_id)
VALUES (0, 'Mock Project 1', 'Description for Mock Project 1', 'PRIVATE', 0);
INSERT INTO PROJECT (id, name, description, visibility, owner_id)
VALUES (1, 'Mock Project 2', 'Description for Mock Project 2', 'PUBLIC', 0);

INSERT INTO TASK (id, title, stage, parent_project_id)
VALUES (0, 'Mock Task 1', 'TEST', 0);