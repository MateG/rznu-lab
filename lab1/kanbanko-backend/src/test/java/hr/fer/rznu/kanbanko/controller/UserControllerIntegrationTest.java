package hr.fer.rznu.kanbanko.controller;

import hr.fer.rznu.kanbanko.model.User;
import hr.fer.rznu.kanbanko.repository.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.jdbc.datasource.init.ResourceDatabasePopulator;

import static org.junit.jupiter.api.Assertions.*;

public class UserControllerIntegrationTest extends AbstractControllerIntegrationTest {

    @Autowired
    private UserRepository userRepository;

    @Test
    public void createUser() {
        User user = User.builder()
                .username("MockUser")
                .password("MockPassword")
                .firstName("Mock")
                .lastName("User")
                .build();
        String body = "{" +
                "\"username\":\"" + user.getUsername() + "\"," +
                "\"password\":\"" + user.getPassword() + "\"," +
                "\"firstName\":\"" + user.getFirstName() + "\"," +
                "\"lastName\":\"" + user.getLastName() + "\"" +
                "}";
        httpRequestExecutor.sendPost("/users", body, false);
        assertTrue(userRepository.exists(Example.of(user, ExampleMatcher.matching().withIgnorePaths("id", "password"))));
    }

    @Test
    public void getUsers() {
        String actual = httpRequestExecutor.sendGet("/users", true);
        String expected = "[{" +
                "\"id\":0," +
                "\"username\":\"MateG\"," +
                "\"firstName\":\"Mate\"," +
                "\"lastName\":\"Gasparini\"" +
                "}]";
        assertEquals(expected, actual);
    }

    @Test
    public void getUser() {
        String actual = httpRequestExecutor.sendGet("/users/0", true);
        String expected = "{" +
                "\"id\":0," +
                "\"username\":\"MateG\"," +
                "\"firstName\":\"Mate\"," +
                "\"lastName\":\"Gasparini\"" +
                "}";
        assertEquals(expected, actual);
    }

    @Test
    public void putModifyUser() {
        User user = User.builder()
                .id(0L)
                .username("MateG_modified")
                .password("Password_modified")
                .firstName("Mate")
                .lastName("Gasparini")
                .build();
        assertTrue(userRepository.existsById(user.getId()));
        String body = "{" +
                "\"username\":\"" + user.getUsername() + "\"," +
                "\"password\":\"" + user.getPassword() + "\"," +
                "\"firstName\":\"" + user.getFirstName() + "\"," +
                "\"lastName\":\"" + user.getLastName() + "\"" +
                "}";
        httpRequestExecutor.sendPut("/users/" + user.getId(), body, true);
        assertTrue(userRepository.exists(Example.of(user, ExampleMatcher.matching().withIgnorePaths("password"))));
    }

    @Test
    public void putCreateUser() {
        User user = User.builder()
                .id(1L)
                .username("MockUser")
                .password("MockPassword123")
                .firstName("Mock")
                .lastName("User")
                .build();
        assertFalse(userRepository.existsById(user.getId()));
        String body = "{" +
                "\"username\":\"" + user.getUsername() + "\"," +
                "\"password\":\"" + user.getPassword() + "\"," +
                "\"firstName\":\"" + user.getFirstName() + "\"," +
                "\"lastName\":\"" + user.getLastName() + "\"" +
                "}";
        httpRequestExecutor.sendPut("/users/" + user.getId(), body, true);
        assertTrue(userRepository.exists(Example.of(user, ExampleMatcher.matching().withIgnorePaths("id", "password"))));
    }

    @Test
    public void deleteUser() {
        assertTrue(userRepository.existsById(0L));
        httpRequestExecutor.sendDelete("/users/0", true);
        assertFalse(userRepository.existsById(0L));
    }
}
