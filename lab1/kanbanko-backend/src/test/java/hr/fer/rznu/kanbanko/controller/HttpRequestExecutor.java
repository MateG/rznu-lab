package hr.fer.rznu.kanbanko.controller;

import lombok.AllArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;

@AllArgsConstructor
public class HttpRequestExecutor {

    private final String apiRoot;

    private final String sessionId;

    public String sendPost(String path, String json, boolean authenticated) {
        RestTemplate restTemplate = new RestTemplate();
        setHeaders(restTemplate, true, authenticated);
        return restTemplate.postForObject(apiRoot + path, json, String.class);
    }

    public String sendGet(String path, boolean authenticated) {
        RestTemplate restTemplate = new RestTemplate();
        setHeaders(restTemplate, false, authenticated);
        return restTemplate.getForObject(apiRoot + path, String.class);
    }

    public void sendPut(String path, String json, boolean authenticated) {
        RestTemplate restTemplate = new RestTemplate();
        setHeaders(restTemplate, true, authenticated);
        restTemplate.put(apiRoot + path, json, String.class);
    }

    public void sendDelete(String path, boolean authenticated) {
        RestTemplate restTemplate = new RestTemplate();
        setHeaders(restTemplate, false, authenticated);
        restTemplate.delete(apiRoot + path);
    }

    private void setHeaders(RestTemplate restTemplate, boolean setJsonContentType, boolean setAuthCookie) {
        restTemplate.setInterceptors(Collections.singletonList((request, body, execution) -> {
            HttpHeaders headers = request.getHeaders();
            if (setJsonContentType) headers.set(HttpHeaders.CONTENT_TYPE, "application/json");
            if (setAuthCookie) headers.set(HttpHeaders.COOKIE, "authentication=" + sessionId);
            return execution.execute(request, body);
        }));
    }
}
