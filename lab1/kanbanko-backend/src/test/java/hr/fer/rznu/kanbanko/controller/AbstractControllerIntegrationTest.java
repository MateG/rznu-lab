package hr.fer.rznu.kanbanko.controller;

import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.HttpStatus;
import org.springframework.jdbc.datasource.init.ResourceDatabasePopulator;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.web.client.HttpStatusCodeException;

import javax.sql.DataSource;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@ActiveProfiles("test")
public abstract class AbstractControllerIntegrationTest {

    protected static Long PLACEHOLDER_ID = 123L;

    protected HttpRequestExecutor httpRequestExecutor;

    @Value("${kanbanko.test.mock-password}")
    protected String mockPassword;

    @Value("http://localhost:${server.port}")
    private String apiRoot;

    @Value("${kanbanko.auth.session-id}")
    private String sessionId;

    @Autowired
    private DataSource dataSource;

    @BeforeEach
    public void setUp() {
        httpRequestExecutor = new HttpRequestExecutor(apiRoot, sessionId);
        ResourceDatabasePopulator populator = new ResourceDatabasePopulator();
        populator.addScript(new ClassPathResource("data.sql"));
        populator.execute(dataSource);
    }

    protected void assertThrowsHttpException(HttpStatus expectedStatusCode, Runnable runnable) {
        try {
            runnable.run();
        } catch (HttpStatusCodeException e) {
            assertEquals(expectedStatusCode, e.getStatusCode());
            return;
        }
        fail("Expected an exception to occur, but no exception occurred.");
    }
}
