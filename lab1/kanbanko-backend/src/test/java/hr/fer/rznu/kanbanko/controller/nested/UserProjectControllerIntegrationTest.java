package hr.fer.rznu.kanbanko.controller.nested;

import hr.fer.rznu.kanbanko.controller.AbstractControllerIntegrationTest;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class UserProjectControllerIntegrationTest extends AbstractControllerIntegrationTest {

    @Test
    public void getUserProjects() {
        String actual = httpRequestExecutor.sendGet("/users/0/projects", true);
        String expected = "[{" +
                "\"id\":0," +
                "\"name\":\"Mock Project 1\"," +
                "\"description\":\"Description for Mock Project 1\"," +
                "\"visibility\":\"PRIVATE\"," +
                "\"ownerId\":0" +
                "},{" +
                "\"id\":1," +
                "\"name\":\"Mock Project 2\"," +
                "\"description\":\"Description for Mock Project 2\"," +
                "\"visibility\":\"PUBLIC\"," +
                "\"ownerId\":0" +
                "}]";
        assertEquals(expected, actual);
    }
}
