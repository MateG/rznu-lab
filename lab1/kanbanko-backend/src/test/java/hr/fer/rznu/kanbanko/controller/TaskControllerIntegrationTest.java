package hr.fer.rznu.kanbanko.controller;

import hr.fer.rznu.kanbanko.model.Project;
import hr.fer.rznu.kanbanko.model.Task;
import hr.fer.rznu.kanbanko.repository.TaskRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;

import static org.junit.jupiter.api.Assertions.*;

public class TaskControllerIntegrationTest extends AbstractControllerIntegrationTest {

    @Autowired
    private TaskRepository taskRepository;

    @Test
    public void createTask() {
        Task task = Task.builder()
                .title("Mock Task Title")
                .stage(Task.Stage.DOING)
                .parentProject(Project.builder().id(0L).build())
                .build();
        String body = "{" +
                "\"title\":\"" + task.getTitle() + "\"," +
                "\"stage\":\"" + task.getStage() + "\"," +
                "\"parentProjectId\":" + task.getParentProject().getId() +
                "}";
        httpRequestExecutor.sendPost("/tasks", body, true);
        assertTrue(taskRepository.exists(Example.of(task, ExampleMatcher.matching().withIgnorePaths("id"))));
    }

    @Test
    public void getTasks() {
        String actual = httpRequestExecutor.sendGet("/tasks", true);
        String expected = "[{" +
                "\"id\":0," +
                "\"title\":\"Mock Task 1\"," +
                "\"stage\":\"TEST\"," +
                "\"parentProjectId\":0" +
                "}]";
        assertEquals(expected, actual);
    }

    @Test
    public void getTask() {
        String actual = httpRequestExecutor.sendGet("/tasks/0", true);
        String expected = "{" +
                "\"id\":0," +
                "\"title\":\"Mock Task 1\"," +
                "\"stage\":\"TEST\"," +
                "\"parentProjectId\":0" +
                "}";
        assertEquals(expected, actual);
    }

    @Test
    public void putModifyTask() {
        Task task = Task.builder()
                .id(0L)
                .title("Mock Task 1 - Modified")
                .stage(Task.Stage.REVIEW)
                .parentProject(Project.builder().id(0L).build())
                .build();
        assertTrue(taskRepository.existsById(task.getId()));
        String body = "{" +
                "\"title\":\"" + task.getTitle() + "\"," +
                "\"stage\":\"" + task.getStage() + "\"," +
                "\"parentProjectId\":" + task.getParentProject().getId() +
                "}";
        httpRequestExecutor.sendPut("/tasks/" + task.getId(), body, true);
        assertTrue(taskRepository.exists(Example.of(task)));
    }

    @Test
    public void putCreateTask() {
        Task task = Task.builder()
                .id(123L)
                .title("Mock Task 123")
                .stage(Task.Stage.DONE)
                .parentProject(Project.builder().id(0L).build())
                .build();
        assertFalse(taskRepository.existsById(task.getId()));
        String body = "{" +
                "\"title\":\"" + task.getTitle() + "\"," +
                "\"stage\":\"" + task.getStage() + "\"," +
                "\"parentProjectId\":" + task.getParentProject().getId() +
                "}";
        httpRequestExecutor.sendPut("/tasks/" + task.getId(), body, true);
        assertTrue(taskRepository.exists(Example.of(task, ExampleMatcher.matching().withIgnorePaths("id"))));
    }

    @Test
    public void deleteTask() {
        assertTrue(taskRepository.existsById(0L));
        httpRequestExecutor.sendDelete("/tasks/0", true);
        assertFalse(taskRepository.existsById(0L));
    }
}
