package hr.fer.rznu.kanbanko.controller;

import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class AuthControllerIntegrationTest extends AbstractControllerIntegrationTest {

    @Test
    public void authSucceedsWhenAuthCookieValid() {
        String actual = httpRequestExecutor.sendGet("/auth", true);
        String expected = "{" +
                "\"id\":0," +
                "\"username\":\"MateG\"," +
                "\"firstName\":\"Mate\"," +
                "\"lastName\":\"Gasparini\"" +
                "}";
        assertEquals(expected, actual);
    }

    @Test
    public void authFailsWhenAuthCookieMissing() {
        Runnable runnable = () -> httpRequestExecutor.sendGet("/auth", false);
        assertThrowsHttpException(HttpStatus.UNAUTHORIZED, runnable);
    }
}
