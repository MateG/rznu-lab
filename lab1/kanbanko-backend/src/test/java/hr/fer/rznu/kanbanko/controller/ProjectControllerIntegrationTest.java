package hr.fer.rznu.kanbanko.controller;

import hr.fer.rznu.kanbanko.model.Project;
import hr.fer.rznu.kanbanko.model.User;
import hr.fer.rznu.kanbanko.repository.ProjectRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;

import static org.junit.jupiter.api.Assertions.*;

public class ProjectControllerIntegrationTest extends AbstractControllerIntegrationTest {

    @Autowired
    private ProjectRepository projectRepository;

    @Test
    public void createProject() {
        Project project = Project.builder()
                .name("Mock Project Name")
                .description("Mock project description")
                .visibility(Project.Visibility.PUBLIC)
                .owner(User.builder().id(0L).build())
                .build();
        String body = "{" +
                "\"name\":\"" + project.getName() + "\"," +
                "\"description\":\"" + project.getDescription() + "\"," +
                "\"visibility\":\"" + project.getVisibility() + "\"," +
                "\"ownerId\":\"" + project.getOwner().getId() + "\"" +
                "}";
        httpRequestExecutor.sendPost("/projects", body, true);
        assertTrue(projectRepository.exists(Example.of(project, ExampleMatcher.matching().withIgnorePaths("id"))));
    }

    @Test
    public void getProjects() {
        String actual = httpRequestExecutor.sendGet("/projects", true);
        String expected = "[{" +
                "\"id\":0," +
                "\"name\":\"Mock Project 1\"," +
                "\"description\":\"Description for Mock Project 1\"," +
                "\"visibility\":\"PRIVATE\"," +
                "\"owner\":{\"id\":0,\"username\":\"MateG\",\"firstName\":\"Mate\",\"lastName\":\"Gasparini\"}" +
                "},{" +
                "\"id\":1," +
                "\"name\":\"Mock Project 2\"," +
                "\"description\":\"Description for Mock Project 2\"," +
                "\"visibility\":\"PUBLIC\"," +
                "\"owner\":{\"id\":0,\"username\":\"MateG\",\"firstName\":\"Mate\",\"lastName\":\"Gasparini\"}" +
                "}]";
        assertEquals(expected, actual);
    }

    @Test
    public void getProject() {
        String actual = httpRequestExecutor.sendGet("/projects/0", true);
        String expected = "{" +
                "\"id\":0," +
                "\"name\":\"Mock Project 1\"," +
                "\"description\":\"Description for Mock Project 1\"," +
                "\"visibility\":\"PRIVATE\"," +
                "\"owner\":{\"id\":0,\"username\":\"MateG\",\"firstName\":\"Mate\",\"lastName\":\"Gasparini\"}" +
                "}";
        assertEquals(expected, actual);
    }

    @Test
    public void putModifyProject() {
        Project project = Project.builder()
                .id(0L)
                .name("Mock Project 1 - Modified")
                .description("Mock project description")
                .visibility(Project.Visibility.PUBLIC)
                .owner(User.builder().id(0L).build())
                .build();
        assertTrue(projectRepository.existsById(project.getId()));
        String body = "{" +
                "\"name\":\"" + project.getName() + "\"," +
                "\"description\":\"" + project.getDescription() + "\"," +
                "\"visibility\":\"" + project.getVisibility() + "\"," +
                "\"ownerId\":\"" + project.getOwner().getId() + "\"" +
                "}";
        httpRequestExecutor.sendPut("/projects/" + project.getId(), body, true);
        assertTrue(projectRepository.exists(Example.of(project, ExampleMatcher.matching().withIgnorePaths("id"))));
    }

    @Test
    public void putCreateProject() {
        Project project = Project.builder()
                .id(123L)
                .name("Mock Project 3")
                .description("Mock project description")
                .visibility(Project.Visibility.PUBLIC)
                .owner(User.builder().id(0L).build())
                .build();
        String body = "{" +
                "\"name\":\"" + project.getName() + "\"," +
                "\"description\":\"" + project.getDescription() + "\"," +
                "\"visibility\":\"" + project.getVisibility() + "\"," +
                "\"ownerId\":\"" + project.getOwner().getId() + "\"" +
                "}";
        httpRequestExecutor.sendPut("/projects/" + project.getId(), body, true);
        assertTrue(projectRepository.exists(Example.of(project, ExampleMatcher.matching().withIgnorePaths("id"))));
    }

    @Test
    public void deleteProject() {
        assertTrue(projectRepository.existsById(0L));
        httpRequestExecutor.sendDelete("/projects/0", true);
        assertFalse(projectRepository.existsById(0L));
    }
}
