package hr.fer.rznu.kanbanko.controller.nested;

import hr.fer.rznu.kanbanko.controller.AbstractControllerIntegrationTest;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ProjectTaskControllerIntegrationTest extends AbstractControllerIntegrationTest {

    @Test
    public void getProjectTasks() {
        String actual = httpRequestExecutor.sendGet("/projects/0/tasks", true);
        String expected = "[{" +
                "\"id\":0," +
                "\"title\":\"Mock Task 1\"," +
                "\"stage\":\"TEST\"," +
                "\"parentProjectId\":0" +
                "}]";
        assertEquals(expected, actual);
    }
}
