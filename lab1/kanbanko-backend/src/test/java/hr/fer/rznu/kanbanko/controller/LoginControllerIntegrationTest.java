package hr.fer.rznu.kanbanko.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class LoginControllerIntegrationTest extends AbstractControllerIntegrationTest {

    @Value("${kanbanko.test.username}")
    private String username;

    @Value("${kanbanko.test.password}")
    private String password;

    @Test
    public void loginSucceeds() {
        String body = "{" +
                "\"username\":\"" + username + "\"," +
                "\"password\":\"" + password +
                "\"}";
        String actual = httpRequestExecutor.sendPost("/login", body, false);
        String expected = "{" +
                "\"id\":0," +
                "\"username\":\"MateG\"," +
                "\"firstName\":\"Mate\"," +
                "\"lastName\":\"Gasparini\"" +
                "}";
        assertEquals(expected, actual);
    }

    @Test
    public void loginFailsWhenCredentialsInvalid() {
        String body = "{" +
                "\"username\":\"" + username + "\"," +
                "\"password\":\"" + mockPassword +
                "\"}";
        Runnable runnable = () -> httpRequestExecutor.sendPost("/login", body, false);
        assertThrowsHttpException(HttpStatus.UNAUTHORIZED, runnable);
    }
}
