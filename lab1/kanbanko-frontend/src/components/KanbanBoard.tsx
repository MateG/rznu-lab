import React from "react";
import { useDispatch } from "react-redux";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import Card from "react-bootstrap/Card";
import CardGroup from "react-bootstrap/CardGroup";
import ListGroup from "react-bootstrap/ListGroup";
import { Check, Pencil, PlusCircle } from "react-bootstrap-icons";
import { Task, TaskStage } from "../types";
import {
  putTask,
  startEditingTaskTitle,
  stopEditingTaskTitle,
  updateTaskTitle,
  startCreatingNewTask,
  updateNewTaskTitle,
  createTask,
} from "../actions/taskActions";

const KanbanBoard: React.FC<{
  projectId: string;
  tasks: Task[];
  allowEditing: boolean;
  editingTaskTitle: boolean;
  editTaskId: string;
  creatingNewTask: boolean;
  newTaskTitle: string;
  newTaskStage: TaskStage;
  handleStageChange: (destinationStage: TaskStage) => (task: Task) => void;
}> = ({
  projectId,
  tasks,
  allowEditing,
  editingTaskTitle,
  editTaskId,
  creatingNewTask,
  newTaskTitle,
  newTaskStage,
  handleStageChange,
}) => (
  <CardGroup>
    <KanbanColumn
      title="To Do"
            stage={TaskStage.TO_DO}
      tasks={tasks}
      projectId={projectId}
      allowEditing={allowEditing}
      editingTaskTitle={editingTaskTitle}
      editTaskId={editTaskId}
      creatingNewTask={creatingNewTask}
      newTaskTitle={newTaskTitle}
      newTaskStage={newTaskStage}
      handleMoveRight={handleStageChange(TaskStage.DOING)}
    />
    <KanbanColumn
      title="Doing"
            stage={TaskStage.DOING}
      tasks={tasks}
      projectId={projectId}
      allowEditing={allowEditing}
      editingTaskTitle={editingTaskTitle}
      editTaskId={editTaskId}
      creatingNewTask={creatingNewTask}
      newTaskTitle={newTaskTitle}
      newTaskStage={newTaskStage}
      handleMoveLeft={handleStageChange(TaskStage.TO_DO)}
      handleMoveRight={handleStageChange(TaskStage.REVIEW)}
    />
    <KanbanColumn
      title="Review"
            stage={TaskStage.REVIEW}
      tasks={tasks}
      projectId={projectId}
      allowEditing={allowEditing}
      editingTaskTitle={editingTaskTitle}
      editTaskId={editTaskId}
      creatingNewTask={creatingNewTask}
      newTaskTitle={newTaskTitle}
      newTaskStage={newTaskStage}
      handleMoveLeft={handleStageChange(TaskStage.DOING)}
      handleMoveRight={handleStageChange(TaskStage.TEST)}
    />
    <KanbanColumn
      title="Test"
            stage={TaskStage.TEST}
      tasks={tasks}
      projectId={projectId}
      allowEditing={allowEditing}
      editingTaskTitle={editingTaskTitle}
      editTaskId={editTaskId}
      creatingNewTask={creatingNewTask}
      newTaskTitle={newTaskTitle}
      newTaskStage={newTaskStage}
      handleMoveLeft={handleStageChange(TaskStage.REVIEW)}
      handleMoveRight={handleStageChange(TaskStage.DONE)}
    />
    <KanbanColumn
      title="Done"
      stage={TaskStage.DONE}
      tasks={tasks}
      projectId={projectId}
      allowEditing={allowEditing}
      editingTaskTitle={editingTaskTitle}
      editTaskId={editTaskId}
      creatingNewTask={creatingNewTask}
      newTaskTitle={newTaskTitle}
      newTaskStage={newTaskStage}
      handleMoveLeft={handleStageChange(TaskStage.TEST)}
    />
  </CardGroup>
);

export default KanbanBoard;

const KanbanColumn: React.FC<{
  title: string;
  stage: TaskStage;
  tasks: Task[];
  projectId: string;
  allowEditing: boolean;
  editingTaskTitle: boolean;
  editTaskId: string;
  creatingNewTask: boolean;
  newTaskTitle: string;
  newTaskStage: TaskStage;
  handleMoveLeft?: (task: Task) => void;
  handleMoveRight?: (task: Task) => void;
}> = ({
  title,
  stage,
  tasks,
  projectId,
  allowEditing,
  editingTaskTitle,
  editTaskId,
  creatingNewTask,
  newTaskTitle,
  newTaskStage,
  handleMoveLeft = null,
  handleMoveRight = null,
}) => {
  tasks = tasks.filter((t) => t.stage === stage);
  const dispatch = useDispatch();

  return (
    <Card>
      <Card.Body>
        <Card.Title>{title}</Card.Title>
        <Card.Subtitle className="mb-2 text-muted">
          {tasks.length} task(s)
        </Card.Subtitle>
        <ListGroup variant="flush">
          {tasks.map((t) => (
            <ListGroup.Item key={t.id}>
              {editingTaskTitle && editTaskId === t.id ? (
                <TaskTitleEdit
                  value={t.title}
                  onChange={(e) =>
                    dispatch(updateTaskTitle(editTaskId, e.target.value))
                  }
                  onCheck={() => {
                    dispatch(stopEditingTaskTitle());
                    dispatch(putTask(t));
                  }}
                />
              ) : (
                <TaskTitleView
                  value={t.title}
                  allowEditing={allowEditing}
                  onClick={() => dispatch(startEditingTaskTitle(t.id))}
                />
              )}
              <br />
              <br />
              {allowEditing && handleMoveLeft != null ? (
                <Button
                  size="sm"
                  variant="outline-primary"
                  className="float-left"
                  onClick={() => handleMoveLeft(t)}
                >
                  Move Left
                </Button>
              ) : (
                ""
              )}
              {allowEditing && handleMoveRight != null ? (
                <Button
                  size="sm"
                  variant="outline-primary"
                  className="float-right"
                  onClick={() => handleMoveRight(t)}
                >
                  Move Right
                </Button>
              ) : (
                ""
              )}
            </ListGroup.Item>
          ))}
          &nbsp;
          <br />
          &nbsp;
          {allowEditing ? (
            <NewTaskComponent
              creatingNewTask={creatingNewTask}
              newTaskTitle={newTaskTitle}
              disabled={creatingNewTask && newTaskStage !== stage}
              onClick={() => dispatch(startCreatingNewTask(stage))}
              onChange={(e) => dispatch(updateNewTaskTitle(e.target.value))}
              onCheck={() => dispatch(createTask(newTaskTitle, stage, projectId))}
            />
          ) : (
            ""
          )}
        </ListGroup>
      </Card.Body>
    </Card>
  );
};

const TaskTitleEdit: React.FC<{
  value: string;
  onChange: (e: React.ChangeEvent<HTMLInputElement>) => void;
  onCheck: () => void;
}> = ({ value, onChange, onCheck }) => (
  <Form inline>
    <Form.Control value={value} onChange={onChange} />
    <Button variant="link" onClick={onCheck}>
      <Check />
    </Button>
  </Form>
);

const TaskTitleView: React.FC<{
  value: string;
  allowEditing: boolean;
  onClick: () => void;
}> = ({ value, allowEditing, onClick }) => (
  <p onClick={allowEditing ? onClick : () => {}}>
    {value}{" "}
    {allowEditing ? (
      <Pencil
        className="text-secondary"
        size="14"
        style={{ cursor: "pointer" }}
      />
    ) : (
      ""
    )}
  </p>
);

const NewTaskComponent: React.FC<{
  creatingNewTask: boolean;
  newTaskTitle: string;
  disabled: boolean;
  onClick: () => void;
  onChange: (e: React.ChangeEvent<HTMLInputElement>) => void;
  onCheck: () => void;
}> = ({ creatingNewTask, newTaskTitle, disabled, onClick, onChange, onCheck }) =>
  disabled ? <br/> : (creatingNewTask ? (
    <Form inline>
      <Form.Control value={newTaskTitle} onChange={onChange} />
      <Button variant="link" onClick={onCheck}>
        <Check />
      </Button>
    </Form>
  ) : (
    <div className="text-primary text-center">
      <PlusCircle
        style={{ cursor: "pointer" }}
        onClick={onClick}
      />
    </div>
  ));
