import React from "react";
import { useDispatch } from "react-redux";
import { Link } from "react-router-dom";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import { Check, EyeFill, EyeSlashFill, Pencil } from "react-bootstrap-icons";
import { Project, ProjectVisibility } from "../types";
import {
  putProject,
  startEditingProjectName,
  stopEditingProjectName,
  updateProjectName,
  startEditingProjectDescription,
  stopEditingProjectDescription,
  updateProjectDescription,
} from "../actions/projectActions";

const ProjectOverview: React.FC<{
  project: Project;
  authUserIsOwner: boolean;
  editingProjectName: boolean;
  editingProjectDescription: boolean;
}> = ({
  project,
  authUserIsOwner,
  editingProjectName,
  editingProjectDescription,
}) => {
  const dispatch = useDispatch();
  const { name, description, visibility, owner } = project;

  return (
    <>
      {editingProjectName ? (
        <NameEdit
          value={name}
          onChange={(e) => dispatch(updateProjectName(e.target.value))}
          onCheck={() => {
            dispatch(stopEditingProjectName());
            dispatch(putProject(project));
          }}
        />
      ) : (
        <NameView
          value={name}
          allowEditing={authUserIsOwner}
          onClick={() => dispatch(startEditingProjectName())}
        />
      )}
      {authUserIsOwner && visibility === ProjectVisibility.PRIVATE ? (
        <>
          <br />
          <p className="text-muted ">
            <i>
              <b>Note: </b>
              Only you can see this project as its visibility is set to PRIVATE
            </i>
          </p>
        </>
      ) : (
        ""
      )}
      <hr style={{ height: 1 }} />
      {editingProjectDescription ? (
        <DescriptionEdit
          value={description}
          onChange={(e) => dispatch(updateProjectDescription(e.target.value))}
          onCheck={() => {
            dispatch(stopEditingProjectDescription());
            dispatch(putProject(project));
          }}
        />
      ) : (
        <DescriptionView
          value={description}
          allowEditing={authUserIsOwner}
          onClick={() => dispatch(startEditingProjectDescription())}
        />
      )}
      <hr style={{ height: 1 }} />
      <p>
        Project owner:{" "}
        <Link className="font-weight-bold" to="">
          {owner != null ? owner.username : ""}
        </Link>{" "}
        {authUserIsOwner ? "(You)" : ""}
        <br />
        Project visibility:{" "}
        <VisibilityToggle
          value={visibility}
          allowEditing={authUserIsOwner}
          onClick={() =>
            dispatch(
              putProject({
                ...project,
                visibility:
                  visibility === ProjectVisibility.PRIVATE
                    ? ProjectVisibility.PUBLIC
                    : ProjectVisibility.PRIVATE,
              })
            )
          }
        />
      </p>
    </>
  );
};

export default ProjectOverview;

const NameEdit: React.FC<{
  value: string;
  onChange: (e: React.ChangeEvent<HTMLInputElement>) => void;
  onCheck: () => void;
}> = ({ value, onChange, onCheck }) => (
  <Form inline>
    <Form.Control value={value} onChange={onChange} />
    <Button variant="link" onClick={onCheck}>
      <Check />
    </Button>
  </Form>
);

const NameView: React.FC<{
  value: string;
  allowEditing: boolean;
  onClick: () => void;
}> = ({ value, allowEditing, onClick }) => (
  <h2 onClick={allowEditing ? onClick : () => {}}>
    {value}{" "}
    {allowEditing ? (
      <Pencil
        className="text-secondary"
        size="14"
        style={{ cursor: "pointer" }}
      />
    ) : (
      ""
    )}
  </h2>
);

const DescriptionEdit: React.FC<{
  value: string;
  onChange: (e: React.ChangeEvent<HTMLInputElement>) => void;
  onCheck: () => void;
}> = ({ value, onChange, onCheck }) => (
  <Form inline>
    <Form.Control
      as="textarea"
      style={{ width: "80%" }}
      value={value}
      onChange={onChange}
    />
    <Button variant="link" onClick={onCheck}>
      <Check />
    </Button>
  </Form>
);

const DescriptionView: React.FC<{
  value: string;
  allowEditing: boolean;
  onClick: () => void;
}> = ({ value, allowEditing, onClick }) => (
  <p onClick={allowEditing ? onClick : () => {}}>
    {value}{" "}
    {allowEditing ? (
      <Pencil
        className="text-secondary"
        size="14"
        style={{ cursor: "pointer" }}
      />
    ) : (
      ""
    )}
  </p>
);

const VisibilityToggle: React.FC<{
  value: ProjectVisibility;
  allowEditing: boolean;
  onClick: () => void;
}> = ({ value, allowEditing, onClick }) =>
  value === ProjectVisibility.PUBLIC ? (
    <EyeFill
      style={allowEditing ? { cursor: "pointer" } : {}}
      onClick={allowEditing ? onClick : () => {}}
    />
  ) : (
    <EyeSlashFill
      style={allowEditing ? { cursor: "pointer" } : {}}
      onClick={allowEditing ? onClick : () => {}}
    />
  );
