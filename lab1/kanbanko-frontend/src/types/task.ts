export type Task = {
  id: string;
  title: string;
  stage: TaskStage;
  parentProjectId: string;
};

export enum TaskStage {
  TO_DO = "TO_DO",
  DOING = "DOING",
  REVIEW = "REVIEW",
  TEST = "TEST",
  DONE = "DONE",
}
