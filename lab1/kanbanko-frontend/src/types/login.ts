export type LoginState = {
  username: string;
  password: string;
  hasError: boolean;
  errorMessage: string;
};
