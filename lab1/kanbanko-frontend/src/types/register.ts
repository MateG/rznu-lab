export type RegisterState = {
  username: string;
  password: string;
  firstName: string;
  lastName: string;
};
