import { Task, TaskStage, User } from "./index";

export type ProjectState = {
  projectLoading: boolean;
  project: Project;
  editingProjectName: boolean;
  editingProjectDescription: boolean;
  tasksLoading: boolean;
  tasks: Task[];
  editingTaskTitle: boolean;
  editTaskId: string;
  creatingNewTask: boolean;
  newTaskTitle: string;
  newTaskStage: TaskStage;
};

export type Project = {
  id: string;
  name: string;
  description: string;
  visibility: ProjectVisibility;
  owner: User | null;
};

export enum ProjectVisibility {
  PRIVATE = "PRIVATE",
  PUBLIC = "PUBLIC",
}
