import { User } from "./index";

export enum AuthStatus {
  UNKNOWN,
  LOADING,
  LOGGED_IN,
  LOGGED_OUT,
}

export type AuthState = {
  status: AuthStatus;
  user: User;
};
