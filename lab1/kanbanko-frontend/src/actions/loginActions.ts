import axios from "axios";
import { LoginState } from "../types/login";

const apiLogin = (username: string, password: string) =>
  axios.post(
    "http://localhost:8080/login",
    {
      username,
      password,
    },
    {
      withCredentials: true,
    }
  );

export const UPDATE_LOGIN_USERNAME = "UPDATE_LOGIN_USERNAME";
export const UPDATE_LOGIN_PASSWORD = "UPDATE_LOGIN_PASSWORD";
export const LOGIN_SUCCESS = "LOGIN_SUCCESS";
export const LOGIN_FAILURE = "LOGIN_FAILURE";

export const updateUsername = (username: string) => ({
  type: UPDATE_LOGIN_USERNAME,
  payload: username,
});

export const updatePassword = (password: string) => ({
  type: UPDATE_LOGIN_PASSWORD,
  payload: password,
});

export const performLogin = (username: string, password: string) => {
  return (dispatch: any) =>
    apiLogin(username, password).then(
      (res) => dispatch(loginSuccess(res)),
      (err) => dispatch(loginFailure())
    );
};

export const loginSuccess = (res: { data: LoginState }) => ({
  type: LOGIN_SUCCESS,
  payload: res.data,
});

export const loginFailure = () => ({
  type: LOGIN_FAILURE,
});

const apiLogout = () =>
  axios.get("http://localhost:8080/logout", {
    withCredentials: true,
  });

export const LOGOUT_SUCCESS = "LOGOUT_SUCCESS";
export const LOGOUT_FAILURE = "LOGOUT_FAILURE";

export const performLogout = () => {
  return (dispatch: any) =>
    apiLogout().then(
      (res) => dispatch(logoutSuccess()),
      (err) => dispatch(logoutFailure())
    );
};

export const logoutSuccess = () => ({
  type: LOGOUT_SUCCESS,
});

export const logoutFailure = () => ({
  type: LOGOUT_FAILURE,
});
