import axios from "axios";

const apiAuth = () =>
  axios.get("http://localhost:8080/auth", {
    withCredentials: true,
  });

export const AUTH_START = "AUTH_START";
export const AUTH_SUCCESS = "AUTH_SUCCESS";
export const AUTH_FAILURE = "AUTH_FAILURE";

export const performAuth = () => {
  return (dispatch: any) => {
    dispatch(authLoading());
    return apiAuth().then(
      (res) => dispatch(authSuccess(res)),
      (err) => dispatch(authFailure())
    );
  };
};

export const authLoading = () => ({
  type: AUTH_START,
});

export const authSuccess = (res: any) => ({
  type: AUTH_SUCCESS,
  payload: res.data,
});

export const authFailure = () => ({
  type: AUTH_FAILURE,
});
