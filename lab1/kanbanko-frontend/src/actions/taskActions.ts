import axios from "axios";
import { Task, TaskStage } from "../types";

export const EDIT_TASK_TITLE_START = "EDIT_TASK_TITLE_START";
export const EDIT_TASK_TITLE_STOP = "EDIT_TASK_TITLE_STOP";

export const startEditingTaskTitle = (taskId: string) => ({
  type: EDIT_TASK_TITLE_START,
  payload: taskId,
});

export const stopEditingTaskTitle = () => ({
  type: EDIT_TASK_TITLE_STOP,
});

export const UPDATE_TASK_TITLE = "UPDATE_TASK_TITLE";

export const updateTaskTitle = (taskId: string, title: string) => ({
  type: UPDATE_TASK_TITLE,
  payload: { taskId, title },
});

export const START_CREATING_NEW_TASK = "START_CREATING_NEW_TASK";

export const startCreatingNewTask = (stage: TaskStage) => ({
  type: START_CREATING_NEW_TASK,
  payload: stage,
});

export const UPDATE_NEW_TASK_TITLE = "UPDATE_NEW_TASK_TITLE";

export const updateNewTaskTitle = (title: string) => ({
  type: UPDATE_NEW_TASK_TITLE,
  payload: title,
});

const apiPostTask = (title: string, stage: TaskStage, parentProjectId: string) =>
  axios.post(
    "http://localhost:8080/tasks",
    {
      title,
      stage,
      parentProjectId,
    },
    {
      withCredentials: true,
    }
  );

export const CREATE_TASK_START = "CREATE_TASK_START";
export const CREATE_TASK_SUCCESS = "CREATE_TASK_SUCCESS";
export const CREATE_TASK_FAILURE = "CREATE_TASK_FAILURE";

export const createTask = (title: string, stage: TaskStage, parentProjectId: string) => {
  return (dispatch: any) => {
    dispatch(createTaskStart());
    return apiPostTask(title, stage, parentProjectId).then(
      (res) => {
        dispatch(createTaskSuccess());
        dispatch(fetchProjectTasks(parentProjectId));
      },
      (err) => dispatch(createTaskFailure())
    );
  };
};

export const createTaskStart = () => ({
  type: CREATE_TASK_START,
});

export const createTaskSuccess = () => ({
  type: CREATE_TASK_SUCCESS,
});

export const createTaskFailure = () => ({
  type: CREATE_TASK_FAILURE,
});

const apiGetProjectTasks = (projectId: string) =>
  axios.get("http://localhost:8080/projects/" + projectId + "/tasks", {
    withCredentials: true,
  });

export const FETCH_TASKS_START = "FETCH_TASKS_START";
export const FETCH_TASKS_SUCCESS = "FETCH_TASKS_SUCCESS";
export const FETCH_TASKS_FAILURE = "FETCH_TASKS_FAILURE";

export const fetchProjectTasks = (projectId: string) => {
  return (dispatch: any) => {
    dispatch(fetchTasksStart());
    return apiGetProjectTasks(projectId).then(
      (res) => dispatch(fetchTasksSuccess(res)),
      (err) => dispatch(fetchTasksFailure())
    );
  };
};

export const fetchTasksStart = () => ({
  type: FETCH_TASKS_START,
});

export const fetchTasksSuccess = (res: { data: Task[] }) => ({
  type: FETCH_TASKS_SUCCESS,
  payload: res.data,
});

export const fetchTasksFailure = () => ({
  type: FETCH_TASKS_FAILURE,
});

const apiPutTask = (task: Task) =>
  axios.put(
    "http://localhost:8080/tasks/" + task.id,
    {
      title: task.title,
      stage: task.stage,
      parentProjectId: task.parentProjectId,
    },
    {
      withCredentials: true,
    }
  );

export const PUT_TASK_START = "PUT_TASK_START";
export const PUT_TASK_SUCCESS = "PUT_TASK_SUCCESS";
export const PUT_TASK_FAILURE = "PUT_TASK_FAILURE";

export const putTask = (task: Task) => {
  return (dispatch: any) => {
    dispatch(putTaskStart());
    return apiPutTask(task).then(
      (res) => {
        dispatch(putTaskSuccess());
        dispatch(fetchProjectTasks(task.parentProjectId));
      },
      (err) => dispatch(putTaskFailure())
    );
  };
};

export const putTaskStart = () => ({
  type: PUT_TASK_START,
});

export const putTaskSuccess = () => ({
  type: PUT_TASK_SUCCESS,
});

export const putTaskFailure = () => ({
  type: PUT_TASK_FAILURE,
});
