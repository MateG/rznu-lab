import axios from "axios";

const apiGetUserProjects = (userId: string) =>
  axios.get("http://localhost:8080/users/" + userId + "/projects", {
    withCredentials: true,
  });

export const USER_PROJECTS_FETCH_START = "USER_PROJECTS_FETCH_START";
export const USER_PROJECTS_FETCH_SUCCESS = "USER_PROJECTS_FETCH_SUCCESS";
export const USER_PROJECTS_FETCH_FAILURE = "USER_PROJECTS_FETCH_FAILURE";

export const fetchUserProjects = (userId: string) => {
  return (dispatch: any) => {
    dispatch(userProjectFetchStart());
    return apiGetUserProjects(userId).then(
      (res) => dispatch(userProjectFetchSuccess(res)),
      (err) => dispatch(userProjectFetchFailure())
    );
  };
};

export const userProjectFetchStart = () => ({
  type: USER_PROJECTS_FETCH_START,
});

export const userProjectFetchSuccess = (res: any) => ({
  type: USER_PROJECTS_FETCH_SUCCESS,
  payload: res.data,
});

export const userProjectFetchFailure = () => ({
  type: USER_PROJECTS_FETCH_FAILURE,
});
