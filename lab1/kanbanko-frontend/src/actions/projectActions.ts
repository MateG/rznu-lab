import axios from "axios";
import { Project, ProjectVisibility } from "../types";

const apiGetProject = (projectId: string) =>
  axios.get("http://localhost:8080/projects/" + projectId, {
    withCredentials: true,
  });

export const FETCH_PROJECT_START = "FETCH_PROJECT_START";
export const FETCH_PROJECT_SUCCESS = "FETCH_PROJECT_SUCCESS";
export const FETCH_PROJECT_FAILURE = "FETCH_PROJECT_FAILURE";

export const fetchProject = (projectId: string) => {
  return (dispatch: any) => {
    dispatch(fetchProjectStart());
    return apiGetProject(projectId).then(
      (res) => dispatch(fetchProjectSuccess(res)),
      (err) => dispatch(fetchProjectFailure())
    );
  };
};

export const fetchProjectStart = () => ({
  type: FETCH_PROJECT_START,
});

export const fetchProjectSuccess = (res: { data: Project }) => ({
  type: FETCH_PROJECT_SUCCESS,
  payload: res.data,
});

export const fetchProjectFailure = () => ({
  type: FETCH_PROJECT_FAILURE,
});

const apiGetProjects = () =>
  axios.get("http://localhost:8080/projects", {
    withCredentials: true,
  });

export const PROJECTS_FETCH_START = "PROJECTS_FETCH_START";
export const PROJECTS_FETCH_SUCCESS = "PROJECTS_FETCH_SUCCESS";
export const PROJECTS_FETCH_FAILURE = "PROJECTS_FETCH_FAILURE";

export const fetchProjects = () => {
  return (dispatch: any) => {
    dispatch(projectFetchStart());
    return apiGetProjects().then(
      (res) => dispatch(projectFetchSuccess(res)),
      (err) => dispatch(projectFetchFailure())
    );
  };
};

export const projectFetchStart = () => ({
  type: PROJECTS_FETCH_START,
});

export const projectFetchSuccess = (res: any) => ({
  type: PROJECTS_FETCH_SUCCESS,
  payload: res.data,
});

export const projectFetchFailure = () => ({
  type: PROJECTS_FETCH_FAILURE,
});

export const EDIT_PROJECT_NAME_START = "EDIT_PROJECT_NAME_START";
export const EDIT_PROJECT_NAME_STOP = "EDIT_PROJECT_NAME_STOP";

export const startEditingProjectName = () => ({
  type: EDIT_PROJECT_NAME_START,
});

export const stopEditingProjectName = () => ({
  type: EDIT_PROJECT_NAME_STOP,
});

export const UPDATE_PROJECT_NAME = "UPDATE_PROJECT_NAME";

export const updateProjectName = (name: string) => ({
  type: UPDATE_PROJECT_NAME,
  payload: name,
});

export const EDIT_PROJECT_DESCRIPTION_START = "EDIT_PROJECT_DESCRIPTION_START";
export const EDIT_PROJECT_DESCRIPTION_STOP = "EDIT_PROJECT_DESCRIPTION_STOP";

export const startEditingProjectDescription = () => ({
  type: EDIT_PROJECT_DESCRIPTION_START,
});

export const stopEditingProjectDescription = () => ({
  type: EDIT_PROJECT_DESCRIPTION_STOP,
});

export const UPDATE_PROJECT_DESCRIPTION = "UPDATE_PROJECT_DESCRIPTION";

export const updateProjectDescription = (description: string) => ({
  type: UPDATE_PROJECT_DESCRIPTION,
  payload: description,
});

export const UPDATE_PROJECT_VISIBILITY = "UPDATE_PROJECT_VISIBILITY";

export const updateProjectVisibility = (visibility: ProjectVisibility) => ({
  type: UPDATE_PROJECT_VISIBILITY,
  payload: visibility,
});

const apiPutProject = (project: Project) =>
  axios.put(
    "http://localhost:8080/projects/" + project.id,
    {
      name: project.name,
      description: project.description,
      visibility: project.visibility,
      ownerId: project.owner == null ? null : project.owner.id,
    },
    {
      withCredentials: true,
    }
  );

export const PUT_PROJECT_START = "PUT_PROJECT_START";
export const PUT_PROJECT_SUCCESS = "PUT_PROJECT_SUCCESS";
export const PUT_PROJECT_FAILURE = "PUT_PROJECT_FAILURE";

export const putProject = (project: Project) => {
  return (dispatch: any) => {
    dispatch(putProjectStart());
    return apiPutProject(project).then(
      (res) => {
        dispatch(putProjectSuccess());
        dispatch(fetchProject(project.id));
      },
      (err) => dispatch(putProjectFailure())
    );
  };
};

export const putProjectStart = () => ({
  type: PUT_PROJECT_START,
});

export const putProjectSuccess = () => ({
  type: PUT_PROJECT_SUCCESS,
});

export const putProjectFailure = () => ({
  type: PUT_PROJECT_FAILURE,
});

export const CREATE_PROJECT_UPDATE_NAME = "CREATE_PROJECT_UPDATE_NAME";

export const updateName = (name: string) => ({
  type: CREATE_PROJECT_UPDATE_NAME,
  payload: name,
});

export const CREATE_PROJECT_UPDATE_DESCRIPTION =
  "CREATE_PROJECT_UPDATE_DESCRIPTION";

export const updateDescription = (description: string) => ({
  type: CREATE_PROJECT_UPDATE_DESCRIPTION,
  payload: description,
});

export const CREATE_PROJECT_UPDATE_VISIBILITY =
  "CREATE_PROJECT_UPDATE_VISIBILITY";

export const updateVisibility = (visibility: string) => ({
  type: CREATE_PROJECT_UPDATE_VISIBILITY,
  payload: visibility,
});

const apiPostProject = (project: Project, ownerId: string) =>
  axios.post(
    "http://localhost:8080/projects",
    {
      name: project.name,
      description: project.description,
      visibility: project.visibility,
      ownerId,
    },
    {
      withCredentials: true,
    }
  );

export const CREATE_PROJECT_START = "CREATE_PROJECT_START";
export const CREATE_PROJECT_SUCCESS = "CREATE_PROJECT_SUCCESS";
export const CREATE_PROJECT_FAILURE = "CREATE_PROJECT_FAILURE";

export const createProject = (project: Project, userId: string) => {
  return (dispatch: any) => {
    dispatch(createProjectStart());
    return apiPostProject(project, userId).then(
      (res) => dispatch(createProjectSuccess()),
      (err) => dispatch(createProjectFailure())
    );
  };
};

export const createProjectStart = () => ({
  type: CREATE_PROJECT_START,
});

export const createProjectSuccess = () => ({
  type: CREATE_PROJECT_SUCCESS,
});

export const createProjectFailure = () => ({
  type: CREATE_PROJECT_FAILURE,
});
