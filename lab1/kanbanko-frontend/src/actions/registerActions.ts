import axios from "axios";

const apiRegister = (
  username: string,
  password: string,
  firstName: string,
  lastName: string
) =>
  axios.post(
    "http://localhost:8080/users",
    {
      username,
      password,
      firstName,
      lastName,
    },
    {
      withCredentials: true,
    }
  );

export const UPDATE_REGISTER_FIRST_NAME = "UPDATE_REGISTER_FIRST_NAME";

export const updateFirstName = (firstName: string) => ({
  type: UPDATE_REGISTER_FIRST_NAME,
  payload: firstName,
});

export const UPDATE_REGISTER_LAST_NAME = "UPDATE_REGISTER_LAST_NAME";

export const updateLastName = (lastName: string) => ({
  type: UPDATE_REGISTER_LAST_NAME,
  payload: lastName,
});

export const UPDATE_REGISTER_USERNAME = "UPDATE_REGISTER_USERNAME";

export const updateUsername = (username: string) => ({
  type: UPDATE_REGISTER_USERNAME,
  payload: username,
});

export const UPDATE_REGISTER_PASSWORD = "UPDATE_REGISTER_PASSWORD";

export const updatePassword = (password: string) => ({
  type: UPDATE_REGISTER_PASSWORD,
  payload: password,
});

export const REGISTER_START = "REGISTER_START";
export const REGISTER_SUCCESS = "REGISTER_SUCCESS";
export const REGISTER_FAILURE = "REGISTER_FAILURE";

export const performRegister = (
  username: string,
  password: string,
  firstName: string,
  lastName: string
) => {
  return (dispatch: any) =>
    apiRegister(username, password, firstName, lastName).then(
      (res) => dispatch(registerSuccess()),
      (err) => dispatch(registerFailure())
    );
};

export const registerSuccess = () => ({
  type: REGISTER_SUCCESS,
});

export const registerFailure = () => ({
  type: REGISTER_FAILURE,
});
