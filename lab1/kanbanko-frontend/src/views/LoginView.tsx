import React from "react";
import { Link } from "react-router-dom";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import { useSelector, useDispatch } from "react-redux";
import { RootState } from "../reducers";
import { LoginState } from "../types";
import {
  updateUsername,
  updatePassword,
  performLogin,
} from "../actions/loginActions";

type Props = {};

const LoginView: React.FC<Props> = () => {
  const {
    username,
    password,
    hasError,
    errorMessage,
  }: LoginState = useSelector((state: RootState) => state.login);

  const dispatch = useDispatch();
  const handleLogin = (e: React.FormEvent<EventTarget>) => {
    e.preventDefault();
    dispatch(performLogin(username, password));
  };

  const inputValid = username.length > 0 && password.length > 0;
  const showErrorMessage = hasError && password.length === 0;

  return (
    <Container>
      <br />
      <h1 className="text-center">Welcome</h1>
      <br />
      <Row>
        <Col md="3" lg="4" />
        <Col>
          <Form onSubmit={handleLogin}>
            <Form.Group>
              <Form.Label>Username</Form.Label>
              <Form.Control
                autoFocus
                value={username}
                onChange={(e) => dispatch(updateUsername(e.target.value))}
              />
            </Form.Group>
            <Form.Group>
              <Form.Label>Password</Form.Label>
              <Form.Control
                type="password"
                value={password}
                isInvalid={showErrorMessage}
                onChange={(e) => dispatch(updatePassword(e.target.value))}
              />
            </Form.Group>
            {errorMessage ? (
              <small className="text-danger">{errorMessage}</small>
            ) : (
              <br />
            )}
            <Button block size="lg" type="submit" disabled={!inputValid}>
              Login
            </Button>
          </Form>
        </Col>
        <Col md="3" lg="4" />
      </Row>
      <br />
      <Row>
        <Col>
          <p className="text-center text-muted">
            Don't have an account? <Link to="register">Register here</Link>
          </p>
        </Col>
      </Row>
    </Container>
  );
};

export default LoginView;
