import React from "react";
import { Link } from "react-router-dom";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import { useSelector, useDispatch } from "react-redux";
import { RootState } from "../reducers";
import { RegisterState } from "../types";
import {
  updateFirstName,
  updateLastName,
  updateUsername,
  updatePassword,
  performRegister,
} from "../actions/registerActions";

type Props = {};

const LoginView: React.FC<Props> = () => {
  const {
    username,
    password,
    firstName,
    lastName,
  }: RegisterState = useSelector((state: RootState) => state.register);

  const dispatch = useDispatch();
  const handleRegister = (e: React.FormEvent<EventTarget>) => {
    e.preventDefault();
    dispatch(performRegister(username, password, firstName, lastName));
  };

  const inputValid =
    username.length > 0 &&
    password.length > 0 &&
    firstName.length > 0 &&
    lastName.length > 0;

  return (
    <Container>
      <br />
      <h2 className="text-center">Create a new account</h2>
      <br />
      <Row>
        <Col md="3" lg="4" />
        <Col>
          <Form onSubmit={handleRegister}>
            <Form.Group>
              <Form.Label>First name</Form.Label>
              <Form.Control
                autoFocus
                value={firstName}
                onChange={(e) => dispatch(updateFirstName(e.target.value))}
              />
            </Form.Group>
            <Form.Group>
              <Form.Label>Last name</Form.Label>
              <Form.Control
                autoFocus
                value={lastName}
                onChange={(e) => dispatch(updateLastName(e.target.value))}
              />
            </Form.Group>
            <Form.Group>
              <Form.Label>Username</Form.Label>
              <Form.Control
                autoFocus
                value={username}
                onChange={(e) => dispatch(updateUsername(e.target.value))}
              />
            </Form.Group>
            <Form.Group>
              <Form.Label>Password</Form.Label>
              <Form.Control
                type="password"
                value={password}
                onChange={(e) => dispatch(updatePassword(e.target.value))}
              />
            </Form.Group>
            <br />
            <Button block size="lg" type="submit" disabled={!inputValid}>
              Register
            </Button>
          </Form>
        </Col>
        <Col md="3" lg="4" />
      </Row>
      <br />
      <Row>
        <Col>
          <p className="text-center text-muted">
            Already have an account? <Link to="">Login here</Link>
          </p>
        </Col>
      </Row>
    </Container>
  );
};

export default LoginView;
