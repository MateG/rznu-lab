import React, { useEffect } from "react";
import { Link } from "react-router-dom";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Table from "react-bootstrap/Table";
import Button from "react-bootstrap/Button";
import { EyeFill, EyeSlashFill } from "react-bootstrap-icons";
import { useSelector, useDispatch } from "react-redux";
import { RootState } from "../reducers";
import { fetchUserProjects } from "../actions/userProjectListActions";
import { Project, ProjectVisibility } from "../types";

type Props = {};

const HomeView: React.FC<Props> = () => {
  const { user } = useSelector((state: RootState) => state.auth);
  const myProjects = useSelector((state: RootState) => state.myProjects);

  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(fetchUserProjects(user.id));
  }, [dispatch, user.id]);

  return (
    <Container fluid>
      <br />
      <br />
      <Row>
        <Col md="1" lg="2" />
        <Col>
          <h1>Hey, {user.firstName}</h1>
          <br />
          {myProjects.length === 0 ? (
            <EmptyProjectsOverview />
          ) : (
            <ProjectsOverview projects={myProjects} />
          )}
          <Link to="/new-project">
            <Button variant="outline-primary">Create a project</Button>
          </Link>
        </Col>
        <Col md="1" lg="2" />
      </Row>
    </Container>
  );
};

const EmptyProjectsOverview: React.FC = () => (
  <p>It looks like you don't have any projects yet.</p>
);

const ProjectsOverview: React.FC<{ projects: Project[] }> = ({ projects }) => (
  <>
    <p>These are the projects you own.</p>
    <Table size="lg">
      <thead>
        <tr>
          <th>Project name</th>
          <th>Project description</th>
          <th>Visibility</th>
        </tr>
      </thead>
      <tbody>
        {projects.map((p: Project) => (
          <tr key={p.id}>
            <td>
              <Link to={"/view-project/" + p.id}>{p.name}</Link>
            </td>
            <td>{p.description}</td>
            <td>
              {p.visibility === ProjectVisibility.PUBLIC ? (
                <EyeFill />
              ) : (
                <EyeSlashFill />
              )}
            </td>
          </tr>
        ))}
      </tbody>
    </Table>
  </>
);

export default HomeView;
