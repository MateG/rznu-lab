import React from "react";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import { useSelector, useDispatch } from "react-redux";
import { RootState } from "../reducers";
import { User, Project, ProjectVisibility } from "../types";
import {
  updateName,
  updateDescription,
  updateVisibility,
  createProject,
} from "../actions/projectActions";

type Props = {};

const CreateProjectView: React.FC<Props> = () => {
  const project: Project = useSelector(
    (state: RootState) => state.createProject
  );
  const user: User = useSelector((state: RootState) => state.auth.user);

  const dispatch = useDispatch();
  const handleCreateProject = (e: React.FormEvent<EventTarget>) => {
    e.preventDefault();
    dispatch(createProject(project, user.id));
  };

  const { name, description, visibility } = project;
  const inputValid = name.length > 0 && description.length > 0;

  return (
    <Container>
      <br />
      <h2 className="text-center">Create a new project</h2>
      <br />
      <Row>
        <Col md="3" lg="4" />
        <Col>
          <Form onSubmit={handleCreateProject}>
            <Form.Group>
              <Form.Label>Project name</Form.Label>
              <Form.Control
                autoFocus
                value={name}
                onChange={(e) => dispatch(updateName(e.target.value))}
              />
            </Form.Group>
            <Form.Group>
              <Form.Label>Project description</Form.Label>
              <Form.Control
                autoFocus
                value={description}
                onChange={(e) => dispatch(updateDescription(e.target.value))}
              />
            </Form.Group>
            <Form.Group>
              <Form.Label>Project visibility</Form.Label>
              <Form.Control
                as="select"
                autoFocus
                value={visibility}
                onChange={(e) => dispatch(updateVisibility(e.target.value))}
              >
                {Object.values(ProjectVisibility).map((v) => (
                  <option key={v}>{v}</option>
                ))}
              </Form.Control>
            </Form.Group>
            <br />
            <Button block size="lg" type="submit" disabled={!inputValid}>
              Create project
            </Button>
          </Form>
        </Col>
        <Col md="3" lg="4" />
      </Row>
    </Container>
  );
};

export default CreateProjectView;
