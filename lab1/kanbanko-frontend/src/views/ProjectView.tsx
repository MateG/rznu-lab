import React, { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { RouteComponentProps } from "react-router";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import { RootState } from "../reducers";
import ProjectOverview from "../components/ProjectOverview";
import KanbanBoard from "../components/KanbanBoard";
import { ProjectState } from "../types";
import { fetchProject } from "../actions/projectActions";
import { fetchProjectTasks, putTask } from "../actions/taskActions";

interface Props extends RouteComponentProps<{ projectId: string }> {}

const CreateProjectView: React.FC<Props> = (props: Props) => {
  const {
    project,
    tasks,
    editingProjectName,
    editingProjectDescription,
    editingTaskTitle,
    editTaskId,
    creatingNewTask,
    newTaskTitle,
    newTaskStage,
  }: ProjectState = useSelector((state: RootState) => state.project);
  const authUser = useSelector((state: RootState) => state.auth.user);
  const projectId = props.match.params.projectId;

  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(fetchProject(projectId));
    dispatch(fetchProjectTasks(projectId));
  }, [dispatch, projectId]);

  if (authUser == null || project.owner == null) {
    return <>Loading...</>;
  }

  const authUserIsOwner = authUser.id === project.owner.id;

  return (
    <Container fluid>
      <br />
      <br />
      <Row>
        <Col md="2" />
        <Col>
          <ProjectOverview
            project={project}
            authUserIsOwner={authUserIsOwner}
            editingProjectName={editingProjectName}
            editingProjectDescription={editingProjectDescription}
          />
        </Col>
        <Col md="2" />
      </Row>
      <KanbanBoard
        projectId={projectId}
        tasks={tasks}
        allowEditing={authUserIsOwner}
        editingTaskTitle={editingTaskTitle}
        editTaskId={editTaskId}
        creatingNewTask={creatingNewTask}
        newTaskTitle={newTaskTitle}
        newTaskStage={newTaskStage}
        handleStageChange={(stage) => (task) =>
          dispatch(putTask({ ...task, stage }))}
      />
    </Container>
  );
};

export default CreateProjectView;
