import React, { useEffect } from "react";
import { Link } from "react-router-dom";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Table from "react-bootstrap/Table";
import Button from "react-bootstrap/Button";
import { useSelector, useDispatch } from "react-redux";
import { RootState } from "../reducers";
import { fetchProjects } from "../actions/projectActions";
import { Project } from "../types";

type Props = {};

const ExploreView: React.FC<Props> = () => {
  const projects = useSelector((state: RootState) => state.projects);

  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(fetchProjects());
  }, [dispatch]);

  return (
    <Container fluid>
      <br />
      <br />
      <Row>
        <Col md="1" lg="2" />
        <Col>
          <h1>Explore projects</h1>
          <br />
          {projects.length === 0 ? (
            <EmptyProjectsOverview />
          ) : (
            <ProjectsOverview projects={projects} />
          )}
          <Link to="/new-project">
            <Button variant="outline-primary">Create a project</Button>
          </Link>
        </Col>
        <Col md="1" lg="2" />
      </Row>
    </Container>
  );
};

const EmptyProjectsOverview: React.FC = () => (
  <p>It looks like there are no projects yet.</p>
);

const ProjectsOverview: React.FC<{ projects: Project[] }> = ({ projects }) => (
  <>
    <p>These are all the projects you have access to.</p>
    <Table size="lg">
      <thead>
        <tr>
          <th>Project name</th>
          <th>Project description</th>
          <th>Owner</th>
        </tr>
      </thead>
      <tbody>
        {projects
          .filter((p) => p != null)
          .map((p: Project) => (
            <tr key={p.id}>
              <td>
                <Link to={"/view-project/" + p.id}>{p.name}</Link>
              </td>
              <td>{p.description}</td>
              <td>{p.owner == null ? "" : p.owner.username}</td>
            </tr>
          ))}
      </tbody>
    </Table>
  </>
);

export default ExploreView;
