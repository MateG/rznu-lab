import { useEffect } from "react";
import {
  BrowserRouter as Router,
  Redirect,
  Route,
  Switch,
} from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import { RootState } from "./reducers";
import { AuthStatus } from "./types";
import { performAuth } from "./actions/authActions";
import { Routes } from "./navigation/routes";
import ProtectedNavigation from "./navigation/ProtectedNavigation";
import PublicNavigation from "./navigation/PublicNavigation";
import Login from "./views/LoginView";
import Register from "./views/RegisterView";
import Home from "./views/HomeView";
import Explore from "./views/ExploreView";
import CreateProject from "./views/CreateProjectView";
import Project from "./views/ProjectView";

const App: React.FC = (): JSX.Element => {
  const auth = useSelector((state: RootState) => state.auth);

  const dispatch = useDispatch();
  useEffect(() => {
    if (auth.status === AuthStatus.UNKNOWN) {
      dispatch(performAuth());
    }
  });

  switch (auth.status) {
    case AuthStatus.UNKNOWN:
    case AuthStatus.LOADING:
      return <>Loading...</>;
    case AuthStatus.LOGGED_IN:
      return <ProtectedApp />;
    case AuthStatus.LOGGED_OUT:
      return <PublicApp />;
  }
};

const PublicApp: React.FC = () => (
  <Router>
    <PublicNavigation />
    <Switch>
      <Route exact path={Routes.LOGIN} component={Login} />
      <Route exact path={Routes.REGISTER} component={Register} />

      <Redirect from={Routes.ROOT} to={Routes.LOGIN} />
    </Switch>
  </Router>
);

const ProtectedApp: React.FC = () => (
  <Router>
    <ProtectedNavigation />
    <Switch>
      <Route exact path={Routes.HOME} component={Home} />
      <Route exact path={Routes.EXPLORE} component={Explore} />
      <Route exact path={Routes.NEW_PROJECT} component={CreateProject} />
      <Route
        exact
        path={Routes.VIEW_PROJECT + "/:projectId"}
        component={Project}
      />

      <Redirect from={Routes.ROOT} to={Routes.HOME} />
    </Switch>
  </Router>
);

export default App;
