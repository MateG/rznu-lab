import { RegisterState } from "../types";
import {
  UPDATE_REGISTER_FIRST_NAME,
  UPDATE_REGISTER_LAST_NAME,
  UPDATE_REGISTER_USERNAME,
  UPDATE_REGISTER_PASSWORD,
  REGISTER_SUCCESS,
  REGISTER_FAILURE,
} from "../actions/registerActions";

const initialState: RegisterState = {
  username: "",
  password: "",
  firstName: "",
  lastName: "",
};

export const registerReducer = (state = initialState, action: any) => {
  switch (action.type) {
    case UPDATE_REGISTER_FIRST_NAME:
      return { ...state, firstName: action.payload };
    case UPDATE_REGISTER_LAST_NAME:
      return { ...state, lastName: action.payload };
    case UPDATE_REGISTER_USERNAME:
      return { ...state, username: action.payload };
    case UPDATE_REGISTER_PASSWORD:
      return { ...state, password: action.payload };
    case REGISTER_SUCCESS:
    case REGISTER_FAILURE:
      return initialState;
    default:
      return state;
  }
};
