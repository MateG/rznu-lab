import { LoginState } from "../types";
import {
  UPDATE_LOGIN_USERNAME,
  UPDATE_LOGIN_PASSWORD,
  LOGIN_SUCCESS,
  LOGIN_FAILURE,
} from "../actions/loginActions";

const initialState: LoginState = {
  username: "",
  password: "",
  hasError: false,
  errorMessage: "",
};

export const loginReducer = (state = initialState, action: any) => {
  switch (action.type) {
    case UPDATE_LOGIN_USERNAME:
      return { ...state, username: action.payload };
    case UPDATE_LOGIN_PASSWORD:
      return { ...state, password: action.payload };
    case LOGIN_SUCCESS:
      return initialState;
    case LOGIN_FAILURE:
      return {
        ...state,
        password: "",
        hasError: true,
        errorMessage: "Invalid username or password.",
      };
    default:
      return state;
  }
};
