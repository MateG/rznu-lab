import { Project } from "../types";
import { USER_PROJECTS_FETCH_SUCCESS } from "../actions/userProjectListActions";

const initialState: Project[] = [];

export const userProjectListReducer = (state = initialState, action: any) => {
  switch (action.type) {
    case USER_PROJECTS_FETCH_SUCCESS:
      return action.payload;
    default:
      return state;
  }
};
