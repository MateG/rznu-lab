import { AuthState, AuthStatus } from "../types";
import { AUTH_START, AUTH_SUCCESS, AUTH_FAILURE } from "../actions/authActions";
import { LOGIN_SUCCESS, LOGOUT_SUCCESS } from "../actions/loginActions";

const initialState = {
  status: AuthStatus.UNKNOWN,
  user: {
    id: "",
    username: "",
    firstName: "",
    lastName: "",
  },
};

export const authReducer = (state = initialState, action: any): AuthState => {
  switch (action.type) {
    case AUTH_START:
      return { ...state, status: AuthStatus.LOADING };
    case AUTH_SUCCESS:
    case LOGIN_SUCCESS:
      return { status: AuthStatus.LOGGED_IN, user: action.payload };
    case AUTH_FAILURE:
    case LOGOUT_SUCCESS:
      return { ...initialState, status: AuthStatus.LOGGED_OUT };
    default:
      return state;
  }
};
