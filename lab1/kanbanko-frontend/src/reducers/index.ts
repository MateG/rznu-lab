import { combineReducers } from "redux";
import { authReducer } from "./authReducer";
import { createProjectReducer } from "./createProjectReducer";
import { loginReducer } from "./loginReducer";
import { projectReducer } from "./projectReducer";
import { projectListReducer } from "./projectListReducer";
import { registerReducer } from "./registerReducer";
import { userProjectListReducer } from "./userProjectListReducer";

export const rootReducer = combineReducers({
  auth: authReducer,
  createProject: createProjectReducer,
  login: loginReducer,
  project: projectReducer,
  projects: projectListReducer,
  register: registerReducer,
  myProjects: userProjectListReducer,
});

export type RootState = ReturnType<typeof rootReducer>;
