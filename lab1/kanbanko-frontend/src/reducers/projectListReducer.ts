import { Project } from "../types";
import { PROJECTS_FETCH_SUCCESS } from "../actions/projectActions";

const initialState: Project[] = [];

export const projectListReducer = (state = initialState, action: any) => {
  switch (action.type) {
    case PROJECTS_FETCH_SUCCESS:
      return action.payload;
    default:
      return state;
  }
};
