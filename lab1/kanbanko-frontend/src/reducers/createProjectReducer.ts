import { Project, ProjectVisibility } from "../types";
import {
  CREATE_PROJECT_UPDATE_NAME,
  CREATE_PROJECT_UPDATE_DESCRIPTION,
  CREATE_PROJECT_UPDATE_VISIBILITY,
  CREATE_PROJECT_SUCCESS,
} from "../actions/projectActions";

const initialState: Project = {
  id: "",
  name: "",
  description: "",
  visibility: ProjectVisibility.PRIVATE,
  owner: null,
};

export const createProjectReducer = (state = initialState, action: any) => {
  switch (action.type) {
    case CREATE_PROJECT_UPDATE_NAME:
      return { ...state, name: action.payload };
    case CREATE_PROJECT_UPDATE_DESCRIPTION:
      return { ...state, description: action.payload };
    case CREATE_PROJECT_UPDATE_VISIBILITY:
      return { ...state, visibility: action.payload };
    case CREATE_PROJECT_SUCCESS:
      return initialState;
    default:
      return state;
  }
};
