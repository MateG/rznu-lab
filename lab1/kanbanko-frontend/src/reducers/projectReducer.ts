import { ProjectState, ProjectVisibility, TaskStage } from "../types";
import {
  FETCH_PROJECT_START,
  FETCH_PROJECT_SUCCESS,
  FETCH_PROJECT_FAILURE,
  EDIT_PROJECT_NAME_START,
  EDIT_PROJECT_NAME_STOP,
  UPDATE_PROJECT_NAME,
  EDIT_PROJECT_DESCRIPTION_START,
  EDIT_PROJECT_DESCRIPTION_STOP,
  UPDATE_PROJECT_DESCRIPTION,
  UPDATE_PROJECT_VISIBILITY,
} from "../actions/projectActions";
import {
  FETCH_TASKS_START,
  FETCH_TASKS_SUCCESS,
  FETCH_TASKS_FAILURE,
  EDIT_TASK_TITLE_START,
  EDIT_TASK_TITLE_STOP,
  UPDATE_TASK_TITLE,
  START_CREATING_NEW_TASK,
  UPDATE_NEW_TASK_TITLE,
  CREATE_TASK_SUCCESS
} from "../actions/taskActions";

const initialState: ProjectState = {
  projectLoading: false,
  project: {
    id: "",
    name: "",
    description: "",
    visibility: ProjectVisibility.PRIVATE,
    owner: null,
  },
  editingProjectName: false,
  editingProjectDescription: false,
  tasksLoading: false,
  tasks: [],
  editingTaskTitle: false,
  editTaskId: "",
  creatingNewTask: false,
  newTaskTitle: "",
  newTaskStage: TaskStage.TO_DO,
};

export const projectReducer = (state = initialState, action: any) => {
  switch (action.type) {
    case FETCH_PROJECT_START:
      return { ...state, projectLoading: true };
    case FETCH_PROJECT_SUCCESS:
      return { ...state, projectLoading: false, project: action.payload };
    case FETCH_PROJECT_FAILURE:
      return { ...state, projectLoading: false, project: initialState };
    case FETCH_TASKS_START:
      return { ...state, tasksLoading: true };
    case FETCH_TASKS_SUCCESS:
      return { ...state, tasksLoading: false, tasks: action.payload };
    case FETCH_TASKS_FAILURE:
      return { ...state, tasksLoading: false, tasks: initialState };
    case EDIT_PROJECT_NAME_START:
      return { ...state, editingProjectName: true };
    case EDIT_PROJECT_NAME_STOP:
      return { ...state, editingProjectName: false };
    case UPDATE_PROJECT_NAME:
      return { ...state, project: { ...state.project, name: action.payload } };
    case EDIT_PROJECT_DESCRIPTION_START:
      return { ...state, editingProjectDescription: true };
    case EDIT_PROJECT_DESCRIPTION_STOP:
      return { ...state, editingProjectDescription: false };
    case UPDATE_PROJECT_DESCRIPTION:
      return {
        ...state,
        project: { ...state.project, description: action.payload },
      };
    case UPDATE_PROJECT_VISIBILITY:
      return {
        ...state,
        project: { ...state.project, visibility: action.payload },
      };
    case EDIT_TASK_TITLE_START:
      return { ...state, editingTaskTitle: true, editTaskId: action.payload };
    case EDIT_TASK_TITLE_STOP:
      return { ...state, editingTaskTitle: false, editTaskId: "" };
    case UPDATE_TASK_TITLE:
      return {
        ...state,
        tasks: state.tasks.map((task) =>
          task.id === action.payload.taskId
            ? { ...task, title: action.payload.title }
            : task
        ),
      };
    case START_CREATING_NEW_TASK:
      return {...state, creatingNewTask: true, newTaskTitle: '', newTaskStage: action.payload};
    case UPDATE_NEW_TASK_TITLE:
      return {...state, newTaskTitle: action.payload};
    case CREATE_TASK_SUCCESS:
      return {...state, creatingNewTask: false};
    default:
      return state;
  }
};
