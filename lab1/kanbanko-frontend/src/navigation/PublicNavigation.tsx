import React from "react";
import Navbar from "react-bootstrap/Navbar";

const PublicNavigation: React.FC<{}> = () => (
  <Navbar bg="light">
    <Navbar.Brand>Kanbanko</Navbar.Brand>
  </Navbar>
);

export default PublicNavigation;
