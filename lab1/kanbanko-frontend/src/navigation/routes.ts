export enum Routes {
  ROOT = "/",
  LOGIN = "/login",
  REGISTER = "/register",
  HOME = "/home",
  EXPLORE = "/explore",
  NEW_PROJECT = "/new-project",
  VIEW_PROJECT = "/view-project",
}
