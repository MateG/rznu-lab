import React from "react";
import { Link } from "react-router-dom";
import Navbar from "react-bootstrap/Navbar";
import Nav from "react-bootstrap/Nav";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import { useDispatch } from "react-redux";
import { Routes } from "./routes";
import { performLogout } from "../actions/loginActions";

const ProtectedNavigation: React.FC<{}> = () => {
  const dispatch = useDispatch();
  const handleLogout = () => {
    dispatch(performLogout());
  };

  return (
    <Navbar bg="light" expand="lg">
      <Navbar.Brand>Kanbanko</Navbar.Brand>
      <Navbar.Toggle />
      <Navbar.Collapse>
        <Nav className="mr-auto">
          <Nav.Link as={Link} to={Routes.HOME}>
            Home
          </Nav.Link>
          <Nav.Link as={Link} to={Routes.EXPLORE}>
            Explore
          </Nav.Link>
        </Nav>
        <Form className="ml-auto">
          <Button variant="outline-secondary" onClick={handleLogout}>
            Logout
          </Button>
        </Form>
      </Navbar.Collapse>
    </Navbar>
  );
};

export default ProtectedNavigation;
