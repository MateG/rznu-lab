package hr.fer.rznu.chat.api;

import hr.fer.rznu.chat.beans.ChatState;
import hr.fer.rznu.chat.model.ChatMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.Map;

@RestController
@RequestMapping("/polling")
public class ChatController {

    private final Logger logger = LoggerFactory.getLogger(ChatController.class);

    @Autowired
    private ChatState chatState;

    @GetMapping("/connect")
    @CrossOrigin(origins = "*")
    public ResponseEntity<Map<String, String>> connect() {
        String id = chatState.addChatSubscriber((message, receiverId) -> {
        });
        logger.info("Added polling chat subscriber with ID=" + id);
        return ResponseEntity.ok(Collections.singletonMap("id", id));
    }

    @GetMapping("/disconnect")
    @CrossOrigin(origins = "*")
    public ResponseEntity<Map<String, String>> disconnect(@RequestParam String userId) {
        chatState.removeChatSubscriber(userId);
        logger.info("Removed polling chat subscriber with ID=" + userId);
        return ResponseEntity.ok(Collections.singletonMap("id", userId));
    }

    @PostMapping("")
    @CrossOrigin(origins = "*")
    public ResponseEntity<ChatMessage> sendMessage(@RequestBody ChatMessage message) {
        chatState.broadcastMessage(message);
        logger.info("Broadcast message: " + message);
        return ResponseEntity.ok(message);
    }

    @GetMapping("")
    @CrossOrigin(origins = "*")
    public ResponseEntity<ChatMessage> getLastMessage(@RequestParam String userId) {
        ChatMessage lastUnreadMessage = chatState.getLastUnreadMessage(userId);
        if (lastUnreadMessage == null) {
            return ResponseEntity.noContent().build();
        } else {
            logger.info("Read previously unread message for user with ID=" + userId);
            return ResponseEntity.ok(lastUnreadMessage);
        }
    }
}
