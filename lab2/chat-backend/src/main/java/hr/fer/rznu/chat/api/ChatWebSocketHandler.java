package hr.fer.rznu.chat.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import hr.fer.rznu.chat.beans.ChatState;
import hr.fer.rznu.chat.model.ChatMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.SubProtocolCapable;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.CopyOnWriteArraySet;

public class ChatWebSocketHandler extends TextWebSocketHandler implements SubProtocolCapable {

    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

    private final Logger logger = LoggerFactory.getLogger(ChatWebSocketHandler.class);

    private final Set<WebSocketSession> sessions = new CopyOnWriteArraySet<>();

    private final Map<String, String> sessionUserIds = new HashMap<>();

    @Autowired
    private ChatState chatState;

    @Override
    public void afterConnectionEstablished(WebSocketSession session) throws IOException {
        sessions.add(session);
        String id = chatState.addChatSubscriber((message, receiverId) -> {
            try {
                String json = OBJECT_MAPPER.writeValueAsString(message);
                session.sendMessage(new TextMessage(json));
            } catch (IOException | IllegalStateException e) {
                logger.info("Removing WebSocket chat subscriber with ID=" + getUserId(session) + " due to send error");
                cleanUpSession(session);
            }
        });
        saveUserId(session, id);
        logger.info("Added WebSocket chat subscriber with ID=" + id);

        String json = OBJECT_MAPPER.writeValueAsString(Collections.singletonMap("id", id));
        session.sendMessage(new TextMessage(json));

        if (chatState.hasUnreadMessages(id)) {
            sendMessageToSession(session, chatState.getLastUnreadMessage(id));
        }
    }

    @Override
    protected void handleTextMessage(WebSocketSession session, TextMessage message) throws IOException {
        ChatMessage chatMessage = OBJECT_MAPPER.readValue(message.getPayload(), ChatMessage.class);
        if (!getUserId(session).equals(chatMessage.getSenderId())) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }

        chatState.broadcastMessage(chatMessage);
        session.sendMessage(message);
        logger.info("Broadcast message: " + chatMessage);
    }

    @Override
    public void handleTransportError(WebSocketSession session, Throwable exception) {
        String userId = getUserId(session);
        cleanUpSession(session);
        logger.info("Removed WebSocket chat subscriber with ID=" + userId + " due to transport error");
    }

    @Override
    public void afterConnectionClosed(WebSocketSession session, CloseStatus status) {
        String userId = getUserId(session);
        cleanUpSession(session);
        logger.info("Removed WebSocket chat subscriber with ID=" + userId + " due to closed connection");
    }

    @Override
    public List<String> getSubProtocols() {
        return Collections.singletonList("subprotocol.demo.websocket");
    }

    private void sendMessageToSession(WebSocketSession session, ChatMessage message) throws IOException {
        String json = OBJECT_MAPPER.writeValueAsString(message);
        session.sendMessage(new TextMessage(json));
    }

    private void cleanUpSession(WebSocketSession session) {
        sessions.remove(session);
        removeUserId(session);
    }

    private void saveUserId(WebSocketSession session, String userId) {
        sessionUserIds.put(session.getId(), userId);
    }

    private void removeUserId(WebSocketSession session) {
        sessionUserIds.remove(session.getId());
    }

    private String getUserId(WebSocketSession session) {
        return sessionUserIds.get(session.getId());
    }
}
