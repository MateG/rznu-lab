package hr.fer.rznu.chat.api;

import hr.fer.rznu.chat.beans.ChatState;
import hr.fer.rznu.chat.model.ChatMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.async.DeferredResult;

import java.util.Collections;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

@RestController
@RequestMapping("/long-polling")
public class ChatLongPollingController {

    private final Logger logger = LoggerFactory.getLogger(ChatController.class);

    @Autowired
    private ChatState chatState;

    @GetMapping("/connect")
    @CrossOrigin(origins = "*")
    public ResponseEntity<Map<String, String>> connect() {
        String id = chatState.addChatSubscriber((message, receiverId) -> {
            synchronized (chatState.getLockByUserId(receiverId)) {
                chatState.getLockByUserId(receiverId).notify();
            }
        });
        logger.info("Added long polling chat subscriber with ID=" + id);
        return ResponseEntity.ok(Collections.singletonMap("id", id));
    }

    @GetMapping("/disconnect")
    @CrossOrigin(origins = "*")
    public ResponseEntity<Map<String, String>> disconnect(@RequestParam String userId) {
        chatState.removeChatSubscriber(userId);
        logger.info("Removed long polling chat subscriber with ID=" + userId);
        return ResponseEntity.ok(Collections.singletonMap("id", userId));
    }

    @PostMapping("")
    @CrossOrigin(origins = "*")
    public ResponseEntity<ChatMessage> sendMessage(@RequestBody ChatMessage message) {
        chatState.broadcastMessage(message);
        logger.info("Broadcast message: " + message);
        return ResponseEntity.ok(message);
    }

    @GetMapping("")
    @CrossOrigin(origins = "*")
    public DeferredResult<ChatMessage> waitForMessage(@RequestParam String userId) {
        DeferredResult<ChatMessage> deferredResult = new DeferredResult<>(100_000L, "Timeout");
        CompletableFuture.runAsync(() -> {
            try {
                synchronized (chatState.getLockByUserId(userId)) {
                    while (!chatState.hasUnreadMessages(userId)) {
                        chatState.getLockByUserId(userId).wait();
                    }
                    deferredResult.setResult(chatState.getLastUnreadMessage(userId));
                }
                logger.info("Read previously unread message for user with ID=" + userId);
            } catch (InterruptedException ignored) {
            }
        });
        return deferredResult;
    }
}
