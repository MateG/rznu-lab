package hr.fer.rznu.chat.util;

import java.security.SecureRandom;
import java.util.Base64;

public final class Util {

    public static String randomId() {
        byte[] bytes = new byte[8];
        new SecureRandom().nextBytes(bytes);
        return Base64.getEncoder().withoutPadding().encodeToString(bytes);
    }
}
