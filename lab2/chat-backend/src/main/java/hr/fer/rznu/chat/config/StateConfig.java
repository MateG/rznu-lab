package hr.fer.rznu.chat.config;

import hr.fer.rznu.chat.beans.ChatState;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class StateConfig {

    @Bean
    public ChatState chatState() {
        return new ChatState();
    }
}
