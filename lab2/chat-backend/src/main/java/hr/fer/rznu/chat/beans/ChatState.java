package hr.fer.rznu.chat.beans;

import hr.fer.rznu.chat.model.ChatMessage;
import lombok.Builder;
import lombok.Data;
import lombok.NonNull;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

import java.util.Objects;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.function.BiConsumer;

import static hr.fer.rznu.chat.util.Util.randomId;

public class ChatState {

    private final Set<ChatConnection> currentConnections = new CopyOnWriteArraySet<>();

    private ChatMessage lastBroadcastMessage;

    public Object getLockByUserId(String userId) {
        return findConnectionByUserId(userId);
    }

    public boolean hasUnreadMessages(String userId) {
        return findConnectionByUserId(userId).lastUnreadMessage != null;
    }

    public String addChatSubscriber(BiConsumer<ChatMessage, String> onReceiveMessage) {
        ChatConnection chatConnection = ChatConnection.builder()
                .userId(randomId())
                .lastUnreadMessage(lastBroadcastMessage)
                .onReceiveMessage(onReceiveMessage)
                .build();
        currentConnections.add(chatConnection);
        return chatConnection.userId;
    }

    public void removeChatSubscriber(String userId) {
        currentConnections.removeIf(c -> c.userId.equals(userId));
        currentConnections.stream()
                .filter(c -> c.lastUnreadMessage != null)
                .filter(c -> c.lastUnreadMessage.getSenderId().equals(userId))
                .forEach(c -> c.lastUnreadMessage = null);
        if (lastBroadcastMessage != null && lastBroadcastMessage.getSenderId().equals(userId)) {
            lastBroadcastMessage = null;
        }
    }

    public void broadcastMessage(ChatMessage message) {
        currentConnections.stream()
                .filter(c -> !c.userId.equals(message.getSenderId()))
                .forEach(c -> c.receiveMessage(message));
        lastBroadcastMessage = message;
    }

    public ChatMessage getLastUnreadMessage(String userId) {
        ChatConnection connection = findConnectionByUserId(userId);
        ChatMessage lastUnreadMessage = connection.lastUnreadMessage;
        connection.lastUnreadMessage = null;
        return lastUnreadMessage;
    }

    private ChatConnection findConnectionByUserId(String userId) {
        return currentConnections.stream()
                .filter(c -> c.userId.equals(userId))
                .findFirst()
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }

    @Data
    @Builder
    private static class ChatConnection {

        @NonNull
        private String userId;

        private ChatMessage lastUnreadMessage;

        private BiConsumer<ChatMessage, String> onReceiveMessage;

        public void receiveMessage(ChatMessage message) {
            onReceiveMessage.accept(message, userId);
            lastUnreadMessage = message;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            ChatConnection that = (ChatConnection) o;
            return userId.equals(that.userId);
        }

        @Override
        public int hashCode() {
            return Objects.hash(userId);
        }
    }
}
