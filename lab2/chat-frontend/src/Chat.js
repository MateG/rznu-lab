import React from "react";
import Button from "react-bootstrap/Button";
import Card from "react-bootstrap/Card";
import Container from "react-bootstrap/Container";
import Form from "react-bootstrap/Form";
import FormControl from "react-bootstrap/FormControl";
import InputGroup from "react-bootstrap/InputGroup";

export default function Chat(props) {
  const [newMessage, setNewMessage] = React.useState("");

  if (props.ownUserId == null) {
    return <React.Fragment>Fetching user ID. Please wait...</React.Fragment>;
  }

  return (
    <React.Fragment>
      <Container
        style={{
          height: "85%",
          overflow: "auto",
          display: "flex",
          flexDirection: "column-reverse",
        }}
      >
        <Container>
          {props.messages.map((message, index) => (
            <Message
              key={index}
              message={message}
              ownUserId={props.ownUserId}
            />
          ))}
        </Container>
      </Container>
      <Form
        onSubmit={(event) => {
          event.preventDefault();
          props.onSendMessage(newMessage);
          setNewMessage("");
        }}
      >
        <InputGroup>
          <FormControl
            placeholder="Type a message..."
            value={newMessage}
            onChange={(event) => setNewMessage(event.target.value)}
          />
          <InputGroup.Append>
            <Button variant="outline-primary" type="submit">
              Send
            </Button>
          </InputGroup.Append>
        </InputGroup>
      </Form>
    </React.Fragment>
  );
}

function Message(props) {
  const isOwnMessage = props.message.senderId === props.ownUserId;
  return (
    <Card
      className={
        "mb-2" +
        (isOwnMessage ? " text-right" : " text-left") +
        (isOwnMessage ? " ml-5" : " mr-5")
      }
      bg={isOwnMessage ? "primary" : "light"}
      text={isOwnMessage ? "white" : "dark"}
      body
    >
      {props.message.text}
    </Card>
  );
}
