export const CommunicationMode = Object.freeze({
  EMPTY: null,
  POLLING: "Polling technique",
  LONG_POLLING: "Long polling technique",
  WEB_SOCKET: "WebSocket protocol",
});
