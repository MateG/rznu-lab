import React from "react";
import Chat from "./Chat";

export default function WebSocketChat(props) {
  const [messages, setMessages] = React.useState([]);
  const [ownUserId, setOwnUserId] = React.useState();
  const [ws, setWs] = React.useState();

  React.useEffect(() => {
    const ws = new WebSocket("ws://localhost:8080/web-socket");
    ws.onmessage = (e) => {
      setOwnUserId(JSON.parse(e.data).id);
      setWs(ws);
    };
  }, []);

  React.useEffect(() => {
    if (ws != null) {
      ws.onmessage = (e) => {
        setMessages((messages) => [...messages, JSON.parse(e.data)]);
      };
    }
  }, [ws]);

  return (
    <Chat
      ownUserId={ownUserId}
      messages={messages}
      onSendMessage={(message) =>
        ws.send(JSON.stringify({ senderId: ownUserId, text: message }))
      }
    />
  );
}
