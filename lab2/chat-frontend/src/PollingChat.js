import React from "react";
import Chat from "./Chat";
import { performXHR } from "./util";

const fetchUserId = (onLoad) => {
  performXHR("GET", "/polling/connect", onLoad);
};

const disconnectUserId = (userId, onLoad = () => {}) => {
  performXHR(
    "GET",
    "/polling/disconnect?userId=" + encodeURIComponent(userId),
    onLoad
  );
};

const getUnreadMessage = (userId, onLoad) => {
  performXHR("GET", "/polling?userId=" + encodeURIComponent(userId), onLoad);
};

const sendMessage = (userId, message, onLoad) => {
  performXHR("POST", "/polling", onLoad, { senderId: userId, text: message });
};

export default function PollingChat(props) {
  const [messages, setMessages] = React.useState([]);
  const [ownUserId, setOwnUserId] = React.useState();

  React.useEffect(() => {
    if (ownUserId == null) {
      fetchUserId((response) => setOwnUserId(response.id));
      return;
    }

    const checkForNewMessages = () => {
      getUnreadMessage(ownUserId, (response) => {
        setMessages((messages) => [...messages, response]);
      });
    };

    checkForNewMessages();
    const intervalId = setInterval(checkForNewMessages, 2000);
    return () => {
      if (ownUserId != null) {
        disconnectUserId(ownUserId);
        setOwnUserId(null);
      }
      if (intervalId != null) {
        clearInterval(intervalId);
      }
    };
  }, [ownUserId]);

  return (
    <Chat
      ownUserId={ownUserId}
      messages={messages}
      onSendMessage={(message) =>
        sendMessage(ownUserId, message, (response) =>
          setMessages([...messages, response])
        )
      }
    />
  );
}
