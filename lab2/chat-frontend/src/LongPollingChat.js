import React from "react";
import Chat from "./Chat";
import { performXHR } from "./util";

const fetchUserId = (onLoad) => {
  performXHR("GET", "/long-polling/connect", onLoad);
};

const disconnectUserId = (userId, onLoad = () => {}) => {
  performXHR(
    "GET",
    "/long-polling/disconnect?userId=" + encodeURIComponent(userId),
    onLoad
  );
};

const getUnreadMessage = (userId, onLoad) => {
  performXHR(
    "GET",
    "/long-polling?userId=" + encodeURIComponent(userId),
    onLoad
  );
};

const sendMessage = (userId, message, onLoad) => {
  performXHR("POST", "/long-polling", onLoad, {
    senderId: userId,
    text: message,
  });
};

export default function LongPollingChat(props) {
  const [messages, setMessages] = React.useState([]);
  const [ownUserId, setOwnUserId] = React.useState();

  React.useEffect(() => {
    if (ownUserId == null) {
      fetchUserId((response) => setOwnUserId(response.id));
      return;
    }

    const checkForNewMessages = () => {
      getUnreadMessage(ownUserId, (response) => {
        setMessages((messages) => [...messages, response]);
        checkForNewMessages();
      });
    };

    checkForNewMessages();
    return () => {
      if (ownUserId != null) {
        disconnectUserId(ownUserId);
        setOwnUserId(null);
      }
    };
  }, [ownUserId]);

  return (
    <Chat
      ownUserId={ownUserId}
      messages={messages}
      onSendMessage={(message) =>
        sendMessage(ownUserId, message, (response) =>
          setMessages([...messages, response])
        )
      }
    />
  );
}
