export const performXHR = (method, path, onLoadIfOk, body = null) => {
  const xhr = new XMLHttpRequest();
  xhr.open(method, "http://localhost:8080" + path);
  xhr.onload = () => {
    if (xhr.status === 200) {
      onLoadIfOk(JSON.parse(xhr.response));
    } else if (xhr.status !== 204) {
      console.log("Error occurred!");
    }
  };
  if (body == null) {
    xhr.send(null);
  } else {
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.send(JSON.stringify(body));
  }
};
