import Button from "react-bootstrap/Button";
import ButtonGroup from "react-bootstrap/ButtonGroup";
import Container from "react-bootstrap/Container";
import { CommunicationMode } from "./enums";

export default function CommunicationModeChooser(props) {
  return (
    <Container className="text-center">
      <p>Please choose communication mode:</p>
      <ButtonGroup vertical>
        <Button
          variant="outline-dark"
          onClick={() => props.onChange(CommunicationMode.POLLING)}
        >
          Polling
        </Button>
        <Button
          variant="outline-dark"
          onClick={() => props.onChange(CommunicationMode.LONG_POLLING)}
        >
          Long polling
        </Button>
        <Button
          variant="outline-dark"
          onClick={() => props.onChange(CommunicationMode.WEB_SOCKET)}
        >
          WebSocket
        </Button>
      </ButtonGroup>
    </Container>
  );
}
