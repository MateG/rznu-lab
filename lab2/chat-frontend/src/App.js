import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import Container from "react-bootstrap/Container";
import CommunicationModeChooser from "./CommunicationModeChooser";
import LongPollingChat from "./LongPollingChat";
import PollingChat from "./PollingChat";
import WebSocketChat from "./WebSocketChat";
import { CommunicationMode } from "./enums";

const modeToComponent = (mode) => {
  switch (mode) {
    case CommunicationMode.POLLING:
      return <PollingChat />;
    case CommunicationMode.LONG_POLLING:
      return <LongPollingChat />;
    default:
      return <WebSocketChat />;
  }
};

export default function App() {
  const [communicationMode, setCommunicationMode] = React.useState(
    CommunicationMode.EMPTY
  );

  if (communicationMode === CommunicationMode.EMPTY) {
    return (
      <Container>
        <h3 className="text-center">Simple chat</h3>
        <br />
        <CommunicationModeChooser onChange={setCommunicationMode} />
      </Container>
    );
  } else {
    return (
      <Container style={{ height: "100%" }}>
        <h3 className="text-center">
          <i>{communicationMode}</i>
        </h3>
        {modeToComponent(communicationMode)}
      </Container>
    );
  }
}
